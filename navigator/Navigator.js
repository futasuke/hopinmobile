import React from 'react';

import { Dimensions, SafeAreaView, StyleSheet } from 'react-native';
import { createAppContainer } from 'react-navigation';
import { createDrawerNavigator } from 'react-navigation-drawer';
import { createStackNavigator } from 'react-navigation-stack';

import HomeScreen from '../view/Home';
import SettingScreen from '../view/Setting';
import LoginScreen from '../view/Login';
import RegisterScreen from '../view/Register';
import FirstDetailScreen from '../view/FirstDetail';
import { ScrollView } from 'react-native-gesture-handler';
import MenuDrawer from '../component/Sidebar';
import EditProfile from '../view/EditProfile';
import MainMap from '../view/MainMap';
import FactScreen from '../view/Fact';
import AdsinfoScreen from '../view/Adsinfo';
import NewsScreen from '../view/News';
import ContactUsScreen from '../view/ContactUs';
import FeedbackScreen from '../view/Feedback';

const screenWidth = Dimensions.get("window").width;

const DrawerNavigatorConfig = {
    initialRouteName: 'Home',
    drawerWidth: screenWidth * 0.7,
    contentComponent: ({ navigation }) => {
        return (<MenuDrawer navigation={navigation} />)
    }
}

const SettingStackConfig = {
    initialRouteName: 'MainSetting',
    headerMode: 'none',
}

const MapStack = createStackNavigator(
    {
        MainMap:{
            screen:MainMap,
        }
    },
    {
        headerMode:'none'
    }
)

const mainDrawer = createDrawerNavigator(
    {
        Home: {
            screen: HomeScreen,
        },
        Setting: {
            screen: SettingScreen,
        },
        Map:{
            screen:MapStack,
        },    
    },
    DrawerNavigatorConfig,
)

const loginStack = createStackNavigator(
    {
        Login: {
            screen: LoginScreen
        },
        Register: {
            screen: RegisterScreen
        },
        FirstDetail: {
            screen: FirstDetailScreen
        },
        Enter: {
            screen: mainDrawer
        },
        EditProfile:{
            screen: EditProfile,
        },
        Fact:{
            screen:FactScreen,
        },
        Adsinfo:{
            screen:AdsinfoScreen,
        },
        News:{
            screen:NewsScreen,
        },
        ContactUs:{
            screen:ContactUsScreen,
        },
        Feedback:{
            screen:FeedbackScreen,
        }
    },
    {
        initialRouteName: 'Login',
        headerMode: "none",
    }
)

// const RootStack = createStackNavigator(
//     {
//         LoginStack:{
//             screen:loginStack,
//         },
//         HomeDrawer:{
//             screen:mainDrawer,
//         },
//         MainSettingStack:{
//             screen:SettingStack,
//         },
//     },
//     {
//         initialRouteName: 'LoginStack',
//         headerMode:'none',
//     }
// )

const CustomDrawerContentComponent = props => (
    <ScrollView>
        <SafeAreaView
            style={styles.container}
            forceInset={{ top: 'always', horizontal: 'never' }}>



        </SafeAreaView>
    </ScrollView>
);

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
})

export default createAppContainer(loginStack);