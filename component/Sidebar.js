import React from 'react';
import { ScrollView, Image, View, Platform, Dimensions, StyleSheet, Text, ImageBackground, Modal, Alert, TouchableOpacity,ActivityIndicator } from 'react-native';
import { StackActions, CommonActions, NavigationEvents } from '@react-navigation/native';
import Axios from 'axios';
import FastImage from 'react-native-fast-image';

const screenWidth = Dimensions.get('window').width
const screenHeight = Dimensions.get('window').height

export default class MenuDrawer extends React.Component {
    state = {
        userId: this.props.route.params.userDetail.id,
        userDetail: this.props.route.params.userDetail,
        ip: this.props.route.params.ip,
        modalVisible: false,
        dpImage: this.props.route.params.dpImage,
        showSpinner:false,
    }

    confirmLogout() {
        this.setState({ modalVisible: true });
    }

    modalAnswer(decision) {
        if (decision == "Yes") {
            this.tryLogout();
        }
        else {
            this.setState({ modalVisible: false });
        }
    }

    tryLogout() {
        this.props.navigation.navigate('Login');
    }

    goToSetting() {
        var self = this;
        this.props.navigation.navigate('Setting', {
            userId: this.state.userDetail.id,
            dpImage: this.state.dpImage,
            ip: this.props.route.params.ip,
            userDetail: this.state.userDetail,
        });
    }

    goToHome() {
        this.props.navigation.navigate('Home');
    }

    updateUser() {
        var self = this;
        Axios.get(this.state.ip + 'api/try-getUser/' + this.state.userId)
            .then(function (response) {
                self.setState({ userDetail: response.data[0].content });
                self.setState({ dpImage: self.state.ip + self.state.userDetail.dpPath });
            }).catch(function (error) {
                console.log(error);
            });
    }

    componentDidMount() {
        var self = this;

        this.onFocus = this.props.navigation.addListener('focus', () => {
            self.updateUser();
        });
        this.onBlur = this.props.navigation.addListener('blur', () => {
            self.setState({ dpImage: "" });
        });
    }

    componentWillUnmount() {
        this.onFocus();
        this.onBlur();
    }

    render() {
        var self = this;
        return (
            <ImageBackground blurRadius={7} source={require('../assets/sidebarbg.jpg')} style={styles.sideBg}>
                <View style={styles.container}>

                    {/* This is for ActivityIndicator aka Spinner */}
                    <Modal
                        animationType="fade"
                        transparent={true}
                        visible={this.state.showSpinner}
                    >
                        <View style={styles.spinnerContainer}>
                            <View style={styles.spinnerModalContent}>
                                <ActivityIndicator size="large" animating={true} color="rgb(251,82,87)" />
                            </View>
                        </View>
                    </Modal>

                    {/* This is logout confirmation modal */}
                    <Modal
                        animationType="fade"
                        transparent={true}
                        visible={this.state.modalVisible}
                    >
                        <View style={styles.logoutModalContainer}>
                            <View style={styles.logoutModalContent}>
                                <View
                                    style={{
                                        borderBottomColor: "rgb(251,82,87)",
                                        borderBottomWidth: 2,
                                        borderStyle: 'solid',
                                        marginBottom: 20,
                                    }}
                                >
                                    <Text
                                        style={{
                                            fontWeight: 'bold',
                                            fontSize: 24,
                                            color: "rgb(251,82,87)",
                                            marginBottom: 5,
                                        }}>ALERT</Text>
                                </View>

                                <Text>Are you sure you want to logout?</Text>

                                <View style={styles.logoutButtonContainer}>

                                    <TouchableOpacity style={styles.logoutNoButton} onPress={() => this.modalAnswer("No")}>
                                        <Text style={{ color: "white", fontWeight: "bold" }}>No</Text>
                                    </TouchableOpacity>

                                    <TouchableOpacity style={styles.logoutYesButton} onPress={() => this.modalAnswer("Yes")}>
                                        <Text style={{ color: "white", fontWeight: "bold" }}>Yes</Text>
                                    </TouchableOpacity>

                                </View>
                            </View>
                        </View>
                    </Modal>


                    {/* This is profile container */}
                    <View style={styles.profileContainer}>
                        <View style={styles.imageContainer}>
                            <FastImage style={styles.profileImage} source={{ uri: this.state.dpImage, cache: FastImage.cacheControl.web, }} />
                        </View>
                        <View style={styles.detailContainer}>
                            <Text style={styles.firstName}>{this.state.userDetail.first_name}</Text>
                            <Text style={styles.lastName}>{this.state.userDetail.last_name}</Text>
                        </View>
                    </View>

                    {/* This is separator */}
                    <View style={styles.separator}>
                        <Text style={{ color: 'white', fontWeight: 'bold' }}>CATEGORY</Text>
                    </View>

                    {/* This is list of link */}
                    <View style={styles.linkContainer}>
                        {/* Home */}
                        <View style={styles.perLinkContainer}>
                            <TouchableOpacity style={styles.linkButton} onPress={() => this.goToHome()}>
                                <Image style={styles.linkIcon} source={require('../assets/Home.png')} />
                                <Text style={styles.linkText}>Home</Text>
                            </TouchableOpacity>
                        </View>
                        {/* Timeline */}
                        <View style={styles.perLinkContainer}>
                            <TouchableOpacity style={styles.linkButton}>
                                <Image style={styles.linkIcon} source={require('../assets/Report.png')} />
                                <Text style={styles.linkText}>Timeline</Text>
                            </TouchableOpacity>
                        </View>
                        {/* Setting */}
                        <View style={styles.perLinkContainer}>
                            <TouchableOpacity style={styles.linkButton} onPress={() => this.goToSetting()}>
                                <Image style={styles.linkIcon} source={require('../assets/Setting.png')} />
                                <Text style={styles.linkText}>Setting</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                    {/* Logout */}
                    <View style={styles.logoutContainer}>
                        <TouchableOpacity style={styles.linkButton} onPress={() => this.confirmLogout()}>
                            <Image style={styles.linkIcon} source={require('../assets/Logout.png')} />
                            <Text style={styles.linkText}>Log Out</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </ImageBackground>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "rgba(251,82,87,0.7)"
    },
    profileContainer: {
        paddingBottom: 20,
        // backgroundColor:"rgb(251,82,87)",
        justifyContent: 'center',
        alignItems: 'center',
    },
    linkContainer: {

    },
    imageContainer: {
        marginBottom: 20,
        marginTop: 20,
        borderRadius: 50,
        overflow: 'hidden',
    },
    profileImage: {
        width: 100,
        height: 100,
        borderRadius: 50,
        borderWidth: 5,
        borderColor: 'white',
    },
    detailContainer: {
        // paddingLeft: 10,
        justifyContent: 'center',
        alignItems: 'center',
    },
    firstName: {
        fontWeight: 'bold',
        fontSize: 26,
        color: 'white',
        // color:"rgb(251,82,87)",
    },
    lastName: {
        color: 'white',
        fontSize: 16,
        // fontWeight:'bold',
    },
    sideBg: {
        width: screenWidth * 0.7,
        height: screenHeight,
        overlayColor: "rgba(0,0,0,0.9)",
    },
    separator: {
        backgroundColor: "rgba(0,0,0,0.2)",
        borderTopColor: 'rgba(255,255,255,0.3)',
        borderBottomColor: 'rgba(255,255,255,0.3)',
        borderBottomWidth: 1,
        borderTopWidth: 1,
        paddingTop: 5,
        paddingBottom: 5,
        paddingLeft: 20,
        // marginBottom:15,
    },
    linkButton: {
        paddingLeft: 20,
        flexDirection: 'row',
    },
    linkText: {
        color: 'white',
        fontSize: 20,
    },
    perLinkContainer: {
        borderBottomColor: 'rgba(255,255,255,0.3)',
        borderBottomWidth: 1,
        paddingBottom: 20,
        paddingTop: 20,
        justifyContent: 'center',
    },
    linkIcon: {
        width: 25,
        height: 25,
        marginRight: 20,
    },
    logoutContainer: {
        position: 'absolute',
        bottom: 30,
        backgroundColor: 'rgba(0,0,0,0.2)',
        width: screenWidth * 0.7,
        borderTopColor: 'rgba(255,255,255,0.3)',
        borderBottomColor: 'rgba(255,255,255,0.3)',
        borderBottomWidth: 1,
        borderTopWidth: 1,
        paddingTop: 20,
        paddingBottom: 20,
    },
    logoutModalContainer: {
        backgroundColor: 'rgba(0,0,0,0.3)',
        alignItems: "center",
        justifyContent: "center",
        flex: 1,
        flexDirection: 'column',
    },
    logoutModalContent: {
        width: 300,
        height: 200,
        padding: 10,
        backgroundColor: 'white',
        borderRadius: 20,
    },
    logoutButtonContainer: {
        // borderColor: 'black',
        // borderWidth: 1,
        height: 60,
        width: 300,
        position: 'absolute',
        bottom: 0,
        // borderRadius: 20,
        flexDirection: "row",
    },
    logoutNoButton: {
        backgroundColor: "rgb(251,82,87)",
        borderRightWidth: 0.5,
        borderRightColor: "rgba(255,255,255,0.5)",
        // borderTopWidth:1,
        // borderTopColor:"rgba(0,0,0,0.5)",
        width: 150,
        bottom: 0,
        justifyContent: "center",
        alignItems: "center",
        borderBottomLeftRadius: 20,
    },
    logoutYesButton: {
        backgroundColor: "rgb(251,82,87)",
        width: 150,
        bottom: 0,
        justifyContent: "center",
        alignItems: "center",
        borderBottomRightRadius: 20,
        borderLeftColor: "rgba(255,255,255,0.5)",
        borderLeftWidth: 0.5,
        // borderTopWidth:1,
        // borderTopColor:"rgba(0,0,0,0.5)",
    },
    spinnerContainer:{
        backgroundColor: 'rgba(0,0,0,0.3)',
        alignItems: "center",
        justifyContent: "center",
        flex: 1,
        flexDirection: 'column',
    },
    spinnerModalContent:{
        alignItems: "center",
        justifyContent: "center",
        width: 200,
        height: 100,
        backgroundColor: 'white',
        borderRadius: 20,
    }
});