import React from 'react';
import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
    View,
    Text,
    StatusBar,
    ImageBackground,
    Dimensions,
    Image,
    TouchableOpacity,
    PermissionsAndroid,
    FlatList,
} from 'react-native';

const screenWidth = Dimensions.get("window").width;

export default class FactCarousel extends React.Component {
    scrollRef = React.createRef();
    state = {
        images: [{ 'image': require('../assets/banner_fact_hopin.png'), id: 1 }, { image: require('../assets/banner_advertise.png'), id: 2 }],
        selectedIndex: 0,
    }

    setSelectedIndex = event => {
        const viewSize = event.nativeEvent.layoutMeasurement.width - 30;
        const contentOffset = event.nativeEvent.contentOffset.x;

        const selectedIndex = Math.floor(contentOffset / viewSize);
        this.setState({ selectedIndex });
    }

    goToFactAds(imageId) {
        if (imageId == 1) {
            this.props.navigation.navigate('Fact');
        }
        if (imageId == 2) {
            this.props.navigation.navigate('Adsinfo');
        }
    }

    componentDidMount() {
        this.autoScroll = setInterval(() => {
            this.setState(prev => ({ selectedIndex: prev.selectedIndex === this.state.images.length - 1 ? 0 : prev.selectedIndex + 1 }), () => {
                this.scrollRef.current.scrollTo({
                    animated: true,
                    y: 0,
                    x: screenWidth * this.state.selectedIndex,
                })
            })
        }, 3000)
    }

    componentWillUnmount() {
        clearInterval(this.autoScroll);
    }

    render() {
        const image = this.state.images;
        const selectIndex = this.state.selectedIndex;
        var self = this;
        return (
            <View style={{ height: 160, width: screenWidth - 50, backgroundColor: 'white', alignSelf: 'center' }}>
                <ScrollView
                    showsHorizontalScrollIndicator={false}
                    scrollEnabled={true}
                    pagingEnabled={true}
                    horizontal
                    onMomentumScrollEnd={this.setSelectedIndex}
                    ref={this.scrollRef}>
                    {image.map(image => (
                        <TouchableOpacity key={image.id} onPress={()=>this.goToFactAds(image.id)}>
                            <Image
                                source={image.image}
                                style={styles.factImage}
                            />
                        </TouchableOpacity>
                    ))}
                </ScrollView>
                <View style={styles.circleContainer}>
                    {image.map((image, i) => (
                        <View
                            key={image.id}
                            style={[styles.whiteCircle, { opacity: i === selectIndex ? 0.5 : 1 }]}
                        />
                    ))}
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    courContainer: {
        flexGrow: 1,
        width: screenWidth - 50,
        height: '100%',
    },
    factImage: {
        width: screenWidth - 50,
        height: 160,
    },
    circleContainer: {
        position: 'absolute',
        bottom: 15,
        height: 10,
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        width: '100%',
    },
    whiteCircle: {
        width: 6,
        height: 6,
        borderRadius: 3,
        margin: 5,
        backgroundColor: 'white'
    },
});