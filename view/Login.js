import React from 'react';
import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
    View,
    Text,
    StatusBar,
    ImageBackground,
    Dimensions,
    Image,
    TouchableOpacity,
    Modal,
    ActivityIndicator,
    KeyboardAvoidingView,
} from 'react-native';

import {
    Input,
    Icon,
    Item,
    Label,
    Left,
} from 'native-base';
import Axios from 'axios';

const screenWidth = Dimensions.get("window").width;
const screenHeight = Dimensions.get("window").height;

export default class LoginScreen extends React.Component {
    state = {
        // email: '',
        // password: '',
        phone: '',
        ip: 'http://192.168.1.107:8080/',
        modalVisible: false,
        alertMsg: '',
        showSpinner: false,
        userId: '',
        userDetail: [],
    }

    checkInput() {
        // if (this.state.email != '') {
        //     if (this.state.password != '') {
        //         this.tryLogin();
        //     }
        //     else {
        //         this.setState({ alertMsg: 'Please enter password' });
        //         this.setState({ modalVisible: true });
        //     }
        // }
        // else {
        //     this.setState({ alertMsg: 'Please enter email' });
        //     this.setState({ modalVisible: true });
        // }

        if (this.state.phone != '') {
            // alert(this.state.phone);
            var str = this.state.phone;

            // check if there is a not-a-number character
            for (var i = 0; i < str.length; i++) {
                if (isNaN(str.charAt(i)) == true) {
                    // alert('ada benda bukan nombor');
                    this.setState({ alertMsg: 'Please enter numbers only (example : 0142957391)' });
                    this.setState({ modalVisible: true });
                    return;
                }
            }

            // if all is number
            this.tryLogin();

        } else {
            this.setState({ alertMsg: 'Please enter phone number' });
            this.setState({ modalVisible: true });
        }
    }

    tryLogin() {
        var self = this;
        this.setState({ showSpinner: true })
        Axios.post(this.state.ip + 'api/try-login', {
            // email: self.state.email,
            // password: self.state.password,
            phone: self.state.phone,
        }).then(function (response) {
            if (response.data[0].message == 'Success') {
                self.setState({ userId: response.data[0].content.id })
                self.setState({ userDetail: response.data[0].content })
                self.setState({ showSpinner: false });
                self.props.navigation.navigate('HomeDrawer', {
                    userId: self.state.userId,
                    userDetail: self.state.userDetail,
                    ip: self.state.ip,
                    dpImage: self.state.ip + self.state.userDetail.dpPath,
                });
            }
            else if (response.data[0].message == 'FirstDetail') {
                self.setState({ userId: response.data[0].content.id })
                self.setState({ showSpinner: false });
                self.props.navigation.navigate('FirstDetail', {
                    userId: self.state.userId,
                    userDetail: self.state.userDetail,
                    ip: self.state.ip,
                });
            }
            else {
                self.setState({ showSpinner: false });
                self.setState({ alertMsg: response.data[0].message });
                self.setState({ modalVisible: true });
            }
        }).catch(function (error) {
            self.setState({ showSpinner: false });
            console.log(error);
        });
    }

    goToRegister() {
        var self = this;
        this.props.navigation.navigate('Register', {
            ip: self.state.ip,
        });
    }

    componentDidMount() {
        console.log(this.state.ip);
    }

    render() {
        return (
            <ImageBackground source={require('../assets/loginBg.png')} style={styles.bgimage}>
                <ScrollView contentContainerStyle={{ alignItems: 'center' }}>
                    <KeyboardAvoidingView behavior="padding" style={styles.container}>

                        <View style={styles.logoContainer}><Image source={require('../assets/logoSmall.png')} style={styles.logo}></Image></View>

                        {/* This is for ActivityIndicator aka Spinner */}
                        <Modal
                            animationType="fade"
                            transparent={true}
                            visible={this.state.showSpinner}
                        >
                            <View style={styles.spinnerContainer}>
                                <View style={styles.spinnerModalContent}>
                                    <ActivityIndicator size="large" animating={true} color="rgb(251,82,87)" />
                                </View>
                            </View>
                        </Modal>

                        {/* This is modal for alert */}
                        <Modal
                            animationType="fade"
                            transparent={true}
                            visible={this.state.modalVisible}
                        >
                            <View style={styles.modalContainer}>
                                <View style={styles.modalContent}>
                                    <View
                                        style={{
                                            borderBottomColor: "rgb(251,82,87)",
                                            borderBottomWidth: 2,
                                            borderStyle: 'solid',
                                            marginBottom: 20,
                                        }}
                                    >
                                        <Text
                                            style={{
                                                fontWeight: 'bold',
                                                fontSize: 24,
                                                color: "rgb(251,82,87)",
                                                marginBottom: 5,
                                            }}>ALERT</Text>
                                    </View>

                                    <Text>{this.state.alertMsg}</Text>

                                    <TouchableOpacity
                                        onPress={() => {
                                            this.setState({ modalVisible: false });
                                        }}
                                        style={styles.hideModalButton}>

                                        <Text
                                            style={{
                                                fontWeight: 'bold',
                                                color: 'white',
                                            }}
                                        >Close</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </Modal>

                        {/* This is container and content of EMAIL input */}
                        <Item rounded style={styles.emailContainer}>
                            <View style={styles.iconContainer}>
                                <Image source={require('../assets/malaysia.png')} style={styles.inputIcon}></Image>
                            </View>
                            <Input
                                placeholder="Phone number"
                                placeholderTextColor="white"
                                style={{ color: 'white' }}
                                keyboardType="phone-pad"
                                onChangeText={(phone) => this.setState({ phone })} />
                        </Item>

                        {/* This is container and content of PASSWORD input */}
                        {/* <Item rounded style={styles.passwordContainer}>
                            <View style={styles.iconContainer}>
                                <Image source={require('../assets/password.png')} style={styles.inputIcon}></Image>
                            </View>
                            <Input
                                placeholder="Password"
                                placeholderTextColor="white"
                                style={{ color: 'white' }}
                                secureTextEntry={true}
                                onChangeText={(password) => this.setState({ password })} />
                        </Item> */}

                        {/* This is login button */}
                        <TouchableOpacity style={styles.loginButton} onPress={() => this.checkInput()}>
                            <Text style={styles.buttonLabel}>Login</Text>
                        </TouchableOpacity>

                        {/* This is sentences for sign up */}
                        <View style={styles.registerContainer}>
                            <Text style={styles.registerText1}>Don't have an account?</Text><TouchableOpacity onPress={() => this.goToRegister()}><Text style={styles.registerText2}>Sign Up Now</Text></TouchableOpacity>
                        </View>

                        {/* This is for forgot password */}
                        <View>
                            <TouchableOpacity>
                                <Text style={styles.forgotText}>Forgot password?</Text>
                            </TouchableOpacity>
                        </View>
                    </KeyboardAvoidingView>
                </ScrollView>
            </ImageBackground>
        );
    }
}

const styles = StyleSheet.create({
    bgimage: {
        width: screenWidth,
        height: screenHeight,
    },
    container: {
        // justifyContent:"center",
        marginTop: 150,
        alignItems: "center",
        flex: 1,
    },
    logo: {
        width: 150,
        height: 150,
        borderRadius: 20,
    },
    logoContainer: {
        borderColor: 'white',
        borderStyle: 'solid',
        borderWidth: 2,
        marginBottom: 100,
        borderRadius: 20,
    },
    emailContainer: {
        width: screenWidth - 100,
        // paddingLeft:20,
        backgroundColor: "rgba(0,0,0,0.2)",
        marginBottom: 80,
    },
    passwordContainer: {
        width: screenWidth - 100,
        // paddingLeft:20,
        backgroundColor: "rgba(0,0,0,0.2)",
        marginBottom: 60,
    },
    iconContainer: {
        backgroundColor: 'white',
        borderRadius: 25,
        width: 60,
        height: 52,
        alignItems: "center",
        justifyContent: "center",
    },
    inputIcon: {
        width: 45,
        height: 30,
    },
    loginButton: {
        backgroundColor: 'white',
        width: screenWidth - 100,
        alignItems: "center",
        justifyContent: "center",
        height: 50,
        borderRadius: 30,
        marginBottom: 100,
    },
    buttonLabel: {
        color: "rgb(251,82,87)",
        fontWeight: 'bold',
    },
    registerContainer: {
        flexDirection: "row",
        marginBottom: 70,
    },
    registerText1: {
        color: 'white',
        fontSize: 18,
        marginRight: 15,
    },
    registerText2: {
        color: "rgb(251,82,87)",
        fontWeight: 'bold',
        fontSize: 18,
    },
    forgotText: {
        color: "white",
        fontWeight: 'bold',
    },
    modalContainer: {
        backgroundColor: 'rgba(0,0,0,0.3)',
        alignItems: "center",
        justifyContent: "center",
        flex: 1,
        flexDirection: 'column',
    },
    modalContent: {
        width: 300,
        height: 200,
        padding: 10,
        backgroundColor: 'white',
        borderRadius: 20,
        // alignItems: "center",
        // justifyContent: "center",
    },
    hideModalButton: {
        position: 'absolute',
        bottom: 10,
        right: 20,
        backgroundColor: "rgb(251,82,87)",
        height: 30,
        width: 60,
        borderRadius: 20,
        alignItems: "center",
        justifyContent: "center",
    },
    spinnerContainer: {
        backgroundColor: 'rgba(0,0,0,0.3)',
        alignItems: "center",
        justifyContent: "center",
        flex: 1,
        flexDirection: 'column',
    },
    spinnerModalContent: {
        alignItems: "center",
        justifyContent: "center",
        width: 200,
        height: 100,
        backgroundColor: 'white',
        borderRadius: 20,
    }
})


