import React from 'react';
import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
    View,
    Text,
    StatusBar,
    ImageBackground,
    Dimensions,
    Image,
    TouchableOpacity,
    Modal,
    ActivityIndicator,
    FlatList,
    RefreshControl,
} from 'react-native';

import {
    Input,
    Icon,
    Item,
    Label,
    Left,
} from 'native-base';
import 'react-native-gesture-handler';
import axios from 'axios';
import FastImage from 'react-native-fast-image';
import MapView, { Marker, AnimatedRegion } from 'react-native-maps';
import MapViewDirections from 'react-native-maps-directions';
import { showLocation } from 'react-native-map-link';

/*
    get
    - user coord,
    - destination coord,
    - path,
*/

const screenWidth = Dimensions.get("window").width;
const screenHeight = Dimensions.get("window").height;

export default class BookMainMap extends React.Component {
    state = {
        ip: this.props.route.params.ip,
        pickupMarker: this.props.route.params.pickupMarker,
        destinationMarker: this.props.route.params.destinationMarker,
        destinationId: this.props.route.params.destinationId,
        userId: this.props.route.params.userId,
        distance: '',
        duration: '',
        ridetype: 'carpool',
        showModal: false,
        showQuestionModal: false,
        showSpinner: false,
        showOfferModal: false,
        radioValue: 'both',
        checkRespond: false,
        requestDetail: '',
        offerArr: [],
        dummyArr: [[
            { driver_id: 37, price: '13.35', id: 28, user: { first_name: 'johe', last_name: 'iskanda' } },
            { driver_id: 47, price: '15.35', id: 2, user: { first_name: 'abu', last_name: 'ali' } },
            { driver_id: 57, price: '15.35', id: 3, user: { first_name: 'abu', last_name: 'ali' } },
            { driver_id: 57, price: '15.35', id: 4, user: { first_name: 'abu', last_name: 'ali' } },
        ]],
        refreshing: false,
        getDriver: false,
        selectedOffer: '',
        showDirection: true,
        oneDriverLocation: '',
        bottomContent: 'select_ride',
        checkStatus: false,
        showRideComplete: false,
        tempOfferData: '',
        tempRouteData: '',
    }

    checkOffer() { //Function for interval
        var self = this;
        console.log(this.state.requestDetail.id);

        if (this.state.checkRespond == true) {
            axios.post(this.state.ip + 'api/try-checkOffer', {
                requestId: this.state.requestDetail.id,
            }).then(function (respond) {
                // console.log(respond.data);
                if (respond.data[0].content != null) {
                    self.setState({ checkRespond: false });
                    setTimeout(() => self.checkMoreOffer(), 10000);
                    // console.log('not null');
                    // console.log(respond.data[0].content);
                    // const tempArr = self.state.offerArr;
                    // tempArr.push(respond.data[0].content);
                    // self.setState({ offerArr: tempArr, showOfferModal: true });
                    // self.testConsole();
                }
            }).catch(function (error) {
                console.log(error);
            });
        }
    }

    checkMoreOffer() { //Function to check more offer after refresh.
        var self = this;
        this.setState({ refreshing: true });
        axios.post(this.state.ip + 'api/try-checkOffer', {
            requestId: this.state.requestDetail.id,
        }).then(function (respond) {
            // console.log(respond.data);
            if (respond.data[0].content != null) {
                self.setState({ checkRespond: false, showSpinner: false, refreshing: false, offerArr: [] });
                console.log('not null');
                console.log(respond.data[0].content);
                const tempArr = self.state.offerArr;
                tempArr.push(respond.data[0].content);
                self.setState({ offerArr: tempArr, showOfferModal: true });
                self.testConsole();
            }
            self.setState({ checkRespond: false, showSpinner: false, refreshing: false });
        }).catch(function (error) {
            console.log(error);
            self.setState({ checkRespond: false, showSpinner: false, refreshing: false });
        });
    }

    testConsole() { //just to test console and state.
        console.log('Offer Array');
        console.log(this.state.offerArr);
    }

    acceptOffer(item) {
        //accept offer and show driver live location. 
        console.log('Accept offer');
        console.log(item);
        this.setState({ tempOfferData: item });
        var self = this;
        axios.post(this.state.ip + 'api/try-acceptOffer', {
            offerId: item.id,
            requestId: item.request_id,
        }).then(function (response) {
            console.log(response);
            self.setState({ getDriver: true, selectedOffer: item, showOfferModal: false, bottomContent: 'driver_otw' });
            self.getOneDriverLocation();
        }).catch(function (error) {
            console.log(error);
        })
    }

    getOneDriverLocation() { //Interval function for getting selected driver location
        var self = this;
        console.log('offer data');
        console.log(this.state.tempOfferData);
        if (this.state.getDriver == true) {
            axios.post(this.state.ip + 'api/try-getOneDriverLocation', {
                driverId: this.state.selectedOffer.driver_id,
                requestId: this.state.tempOfferData.request_id,
            }).then(function (response) {
                console.log(response);
                self.setState({ oneDriverLocation: response.data[0].content, showDirection: false, })
                if (response.data[0].reqStatus.status == 'otw') {
                    self.setState({ showDirection: true, bottomContent: 'destination_otw', getDriver: false, checkStatus: true })
                }
            }).catch(function (error) {
                console.log(error);
            })
        }
    }

    checkRideStatus() {
        var self = this;
        if (this.state.checkStatus == true) {
            axios.post(this.state.ip + 'api/try-rideComplete', {
                reqId: this.state.tempOfferData.request_id,
            }).then(function (respond) {
                console.log(respond);
                if (respond.data == "Complete") {
                    self.setState({ checkStatus: false, showRideComplete: true });
                }
            }).catch(function (error) {
                console.log(error);
            });
        }
    }

    componentDidMount() {
        console.log(this.state.dummyArr);
        this.interval = setInterval(() => this.intervalFunction(), 2000);
    }

    intervalFunction() { //container for all functions that require interval
        this.checkOffer();
        this.getOneDriverLocation();
        this.checkRideStatus();
    }

    componentWillUnmount() {
        clearInterval(this.interval);
    }

    // testConsole() {
    //     console.log('destination');
    //     console.log(this.state.destinationMarker);
    // }

    requestRide() {
        var self = this;
        console.log('Ride requested. Please wait');
        if (this.state.ridetype == 'normal') {
            axios.post(this.state.ip + 'api/try-requestRide', {
                passengerId: this.state.userId,
                pickupCoord: this.state.pickupMarker,
                destinationId: this.state.destinationId,
            }).then(function (respond) {
                console.log(respond);
                self.setState({ requestDetail: respond.data });
                self.setState({ showSpinner: true, checkRespond: true });
            }).catch(function (error) {
                console.log(error);
            });
        }
        if (this.state.ridetype == 'carpool') {
            axios.post(this.state.ip + 'api/try-requestCarpool', {
                passengerId: this.state.userId,
                pickupCoord: this.state.pickupMarker,
                destinationId: this.state.destinationId,
            }).then(function (respond) {
                console.log(respond);
                self.setState({ requestDetail: respond.data });
                self.setState({ showSpinner: true, checkRespond: true });
                self.saveRideRoute();
            }).catch(function (error) {
                console.log(error);
            });
        }
    }

    saveRideRoute() {
        var self = this;
        console.log()
        axios.get(this.state.ip + 'api/try-saveRideRoute/' + this.state.requestDetail.id).then(function (respond) {
            console.log(respond);
        }).catch(function (error) {
            console.log(error);
        })
    }

    cancelSearch() { //cancel offer search
        var self = this;
        axios.post(this.state.ip + 'api/try-cancelSearch', {
            requestId: this.state.requestDetail.id,
        }).then(function (respond) {
            console.log(respond);
            self.setState({ showSpinner: false, checkRespond: false, offerArr: [], showOfferModal: false })
        }).catch(function (error) {
            console.log(error);
        });
    }

    openWaze() {
        var self = this;
        showLocation({
            app: 'waze',
            latitude: self.state.destinationMarker.latitude,
            longitude: self.state.destinationMarker.longitude,
        })
    }

    renderOfferCard = ({ item, index }) => {
        return (
            <TouchableOpacity style={{
                width: '100%',
                height: 80,
                backgroundColor: 'rgba(251,82,87,0.1)',
                borderColor: 'rgb(251,82,87)',
                borderWidth: 2,
                marginBottom: 20,
                borderRadius: 10,
                padding: 10,
                flexDirection: 'row',
                alignItems: 'center',
            }} onPress={() => this.acceptOffer(item)}>
                <View style={{ width: '50%' }}>
                    <Text style={{ fontSize: 20, fontWeight: 'bold' }}>Driver Name: </Text>
                    <Text style={{ fontSize: 18 }}>{item.user.first_name} {item.user.last_name}</Text>
                </View>
                <View>
                    <Text style={{ fontSize: 20, fontWeight: 'bold' }}>Offered price: </Text>
                    <Text style={{ fontSize: 18 }}>{item.price}</Text>
                </View>
            </TouchableOpacity>
        )
    }

    render() {
        var self = this;
        const gender = [
            { key: '1', text: 'Both', value: 'both' },
            { key: '2', text: 'Male', value: 'male' },
            { key: '3', text: 'Female', value: 'female' },
        ];

        const renderRideType = () => {
            if (this.state.ridetype == 'carpool') {
                return (
                    <TouchableOpacity style={styles.buttonPickRide} onPress={() => this.setState({ showModal: true })}>
                        <Image source={require('../../assets/carpool.png')} style={{ width: 40, height: 40, marginRight: 10, }}></Image>
                        <View>
                            <Text style={{ fontSize: 18, fontWeight: 'bold' }}>Carpool</Text>
                            <Text>1-4 seats</Text>
                            <Text>Will pickup passenger that is on the same route</Text>
                        </View>
                    </TouchableOpacity>
                )
            }
            if (this.state.ridetype == 'normal') {
                return (
                    <TouchableOpacity style={styles.buttonPickRide} onPress={() => this.setState({ showModal: true })}>
                        <Image source={require('../../assets/car.png')} style={{ width: 40, height: 40, marginRight: 10, }}></Image>
                        <View>
                            <Text style={{ fontSize: 18, fontWeight: 'bold' }}>Normal ride</Text>
                            <Text>1-4 seats</Text>
                            <Text>Normal car ride. Will not pickup passenger on the way</Text>
                        </View>
                    </TouchableOpacity>
                )
            }
        }

        const renderDirection = () => {
            var self = this;
            if (this.state.showDirection == true) {
                console.log('show direction true');
                return (
                    <MapViewDirections
                        origin={{ latitude: this.state.pickupMarker.latitude, longitude: this.state.pickupMarker.longitude }}
                        destination={{ latitude: this.state.destinationMarker.latitude, longitude: this.state.destinationMarker.longitude }}
                        apikey={'AIzaSyDtafZr2PyN-K1fg-zBwsvqS-W2Cqz1IuI'}
                        strokeWidth={5}
                        strokeColor="rgba(251,82,87,0.8)"
                        onReady={result => {
                            console.log('direction');
                            console.log(result);
                            var num = Math.floor(result.duration);
                            var tempHour = num / 60;
                            var hour = Math.floor(tempHour);
                            var min = num % 60;
                            var combine = "" + hour + " hour(s) " + min + " min(s)";
                            this.setState({ distance: result.distance, duration: combine, tempRouteData: result });
                            this.map.fitToCoordinates(result.coordinates, {
                                edgePadding: {
                                    right: (screenWidth / 100),
                                    bottom: (screenHeight / 100),
                                    left: (screenWidth / 100),
                                    top: (screenHeight / 100),
                                }
                            });
                        }}
                        onError={(errorMessage) => {
                            console.log('errorMessage');
                        }}
                    />
                )
            }
            if (this.state.showDirection == false) { //Else, render driver location
                console.log('show direction false');
                console.log(this.state.oneDriverLocation);
                var lat = parseFloat(this.state.oneDriverLocation.latitude);
                var lng = parseFloat(this.state.oneDriverLocation.longitude);
                // var lat = parseFloat(this.state.testDriverCoord.latitude);
                // var lng = parseFloat(this.state.testDriverCoord.longitude);
                // var lat = parseFloat("3.2350567494824130");
                // var lng = parseFloat("101.7011262662708800");
                return (
                    <Marker
                        coordinate={{ latitude: lat, longitude: lng }}
                        image={require('../../assets/driver_marker.png')}
                        style={{ height: 48, width: 48 }}
                    />
                )

                // console.log(this.state.testDriverCoord);
            }
        }

        const bottomContainer = () => {
            if (this.state.bottomContent == 'select_ride') {
                return (
                    <View style={styles.bookInfoContainer}>
                        <View>
                            <Text style={{ fontSize: 18, fontWeight: 'bold' }}>Select ride type</Text>
                        </View>
                        <View style={{ marginTop: 10, alignItems: 'center' }}>
                            {renderRideType()}
                        </View>
                        <Text style={{ fontSize: 18 }}>Distance : {this.state.distance} km</Text>
                        <Text style={{ fontSize: 18 }}>Duration : {this.state.duration}</Text>
                        <TouchableOpacity style={styles.confirmBookButton} onPress={() => this.requestRide()}>
                            <Text style={{ color: 'white', fontSize: 18, fontWeight: 'bold' }}>Confirm Book</Text>
                        </TouchableOpacity>
                    </View>
                )
            }
            if (this.state.bottomContent == 'driver_otw') {
                return (
                    <View style={[styles.bookInfoContainer, { justifyContent: 'center' }]}>
                        <View style={{ justifyContent: 'center', alignItems: 'center', position: 'absolute', top: 10, alignSelf: 'center' }}>
                            <Text style={{ fontSize: 18, fontWeight: 'bold', }}>Driver is on the way to you. Please wait.</Text>
                        </View>
                        <View style={{ marginTop: 10, alignItems: 'center' }}>
                            <ActivityIndicator size="large" animating={true} color="rgb(251,82,87)" style={{ marginBottom: 15 }} />
                        </View>
                        <TouchableOpacity style={{
                            width: screenWidth - 30,
                            height: 40,
                            backgroundColor: 'rgb(251,82,87)',
                            justifyContent: 'center',
                            alignItems: 'center',
                            borderRadius: 20,
                            position: 'absolute',
                            alignSelf: 'center',
                            bottom: 10,
                        }} onPress={() => this.requestRide()}>
                            <Text style={{ color: 'white', fontSize: 18, fontWeight: 'bold' }}>Cancel Ride</Text>
                        </TouchableOpacity>
                    </View>
                )
            }
            if (this.state.bottomContent == 'destination_otw') {
                return (
                    <View style={[styles.bookInfoContainer, { justifyContent: 'center' }]}>
                        <View style={{ justifyContent: 'center', alignItems: 'center', position: 'absolute', top: 10, alignSelf: 'center' }}>
                            <Text style={{ fontSize: 18, fontWeight: 'bold' }}>You are on the way to your destination</Text>
                        </View>
                        <View style={{ alignItems: 'center' }}>
                            <ActivityIndicator size="large" animating={true} color="rgb(251,82,87)" style={{ marginBottom: 15 }} />
                        </View>
                        {/* <Text style={{ fontSize: 18 }}>Distance : {this.state.distance} km</Text>
                        <Text style={{ fontSize: 18 }}>Duration : {this.state.duration}</Text> */}
                        <TouchableOpacity style={{
                            width: screenWidth - 30,
                            height: 40,
                            backgroundColor: 'rgb(251,82,87)',
                            justifyContent: 'center',
                            alignItems: 'center',
                            borderRadius: 20,
                            position: 'absolute',
                            alignSelf: 'center',
                            bottom: 10,
                        }} onPress={() => this.openWaze()}>
                            <Text style={{ color: 'white', fontSize: 18, fontWeight: 'bold' }}>Show in Waze</Text>
                        </TouchableOpacity>
                    </View>
                )
            }
        }

        return (
            <View style={styles.container}>
                {/* <Text>Ip : {this.state.ip}</Text>
                <Text>pickupMarker Latitude : {this.state.pickupMarker.latitude}</Text>
                <Text>pickupMarker Longitude : {this.state.pickupMarker.longitude}</Text>
                <Text>destinationMarker Latitude : {this.state.destinationMarker.latitude}</Text>
                <Text>destinationMarker Longitude : {this.state.destinationMarker.longitude}</Text> */}

                {/* This is for ActivityIndicator aka Spinner for Waiting for Drivers Responds.*/}
                <Modal
                    animationType="fade"
                    transparent={true}
                    visible={this.state.showSpinner}
                // visible={true}
                >
                    <View style={styles.spinnerContainer}>
                        <View style={styles.spinnerModalContent}>
                            <View>
                                <Text style={{ color: "rgb(251,82,87)", fontWeight: 'bold', marginBottom: 10, marginTop: 15 }}>Waiting for drivers responds.</Text>
                            </View>
                            <ActivityIndicator size="large" animating={true} color="rgb(251,82,87)" style={{ marginBottom: 15 }} />
                            <TouchableOpacity
                                style={{
                                    backgroundColor: "rgb(251,82,87)",
                                    width: '100%',
                                    // flex:1,
                                    justifyContent: 'center',
                                    alignItems: 'center',
                                    height: 40,
                                    borderBottomLeftRadius: 20,
                                    borderBottomRightRadius: 20,
                                }}
                                onPress={() => this.cancelSearch()}
                            >
                                <Text style={{ fontSize: 18, color: 'white', fontWeight: 'bold' }}>Cancel</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </Modal>

                {/* Modal for select ride type */}
                <Modal
                    animationType="fade"
                    transparent={true}
                    visible={this.state.showModal}
                >
                    <View style={styles.modalContainer}>
                        <View style={styles.modalContent}>
                            <Text style={{ fontSize: 18, fontWeight: 'bold' }}>Select Ride Type</Text>
                            <TouchableOpacity style={styles.selectRideContainer} onPress={() => this.setState({ showModal: false, showQuestionModal: true })}>
                                <Image source={require('../../assets/carpool.png')} style={{ width: 40, height: 40, marginRight: 10, }}></Image>
                                <View>
                                    <Text style={{ fontSize: 18, fontWeight: 'bold' }}>Carpool</Text>
                                    <Text>1-4 seats</Text>
                                    <Text>Will pickup passenger that is on the same route</Text>
                                </View>
                            </TouchableOpacity>
                            <TouchableOpacity style={styles.selectRideContainer} onPress={() => this.setState({ showModal: false, ridetype: 'normal' })}>
                                <Image source={require('../../assets/car.png')} style={{ width: 40, height: 40, marginRight: 10, }}></Image>
                                <View>
                                    <Text style={{ fontSize: 18, fontWeight: 'bold' }}>Normal ride</Text>
                                    <Text>1-4 seats</Text>
                                    <Text>Normal car ride. Will not pickup passenger on the way</Text>
                                </View>
                            </TouchableOpacity>
                        </View>
                    </View>
                </Modal>

                {/* Modal for carpool question */}
                <Modal
                    animationType="fade"
                    transparent={true}
                    visible={this.state.showQuestionModal}
                >
                    <View style={styles.modalContainer}>
                        <View style={styles.questionModalContent}>
                            <View>
                                <Text style={{ fontSize: 18, fontWeight: 'bold', fontSize: 18 }}>Carpool Info</Text>
                            </View>

                            <View style={styles.genderContainer}>
                                <View style={{ marginRight: 20 }}>
                                    <Text style={{ color: 'rgb(251,82,87)', fontWeight: 'bold', fontSize: 18 }}>Gender :</Text>
                                </View>
                                {gender.map(item => {
                                    return (
                                        <View key={item.id} style={styles.radioContainer}>
                                            <Text style={{ color: 'rgb(251,82,87)', fontWeight: 'bold', fontSize: 18, marginRight: 10 }}>{item.text}</Text>
                                            <TouchableOpacity style={styles.circle} onPress={() => this.setState({ radioValue: item.value })}>
                                                {this.state.radioValue == item.value && (<View style={styles.checkedCircle} />)}
                                            </TouchableOpacity>
                                        </View>
                                    )
                                })}
                            </View>

                            <TouchableOpacity onPress={() => this.setState({ showQuestionModal: false, ridetype: 'carpool' })} style={[
                                styles.questionConfirmButton, {
                                    position: 'absolute',
                                    bottom: 10,
                                    alignSelf: 'center',
                                }]} >
                                <Text style={{ color: 'white', fontSize: 18, fontWeight: 'bold' }}>Confirm</Text>
                            </TouchableOpacity>

                        </View>
                    </View>

                </Modal>

                {/* Modal for list of offers */}
                <Modal
                    animationType="fade"
                    transparent={true}
                    visible={this.state.showOfferModal}
                // visible={true}
                >
                    <View style={styles.modalContainer}>
                        <View style={styles.offerModalContent}>
                            <View style={{ height: '100%', paddingBottom: 10, width: '100%' }}>
                                <Text style={{ fontSize: 18, fontWeight: 'bold', marginBottom: 20 }}>List of offers</Text>
                                <FlatList
                                    data={this.state.offerArr[0]}
                                    renderItem={this.renderOfferCard}
                                    keyExtractor={item => item.id.toString()}
                                    refreshControl={
                                        <RefreshControl
                                            refreshing={this.state.refreshing}
                                            onRefresh={() => this.checkMoreOffer()}
                                            progressBackgroundColor="rgb(251,82,87)"
                                            colors={["white"]}
                                        />}
                                />
                                <View style={{ marginTop: 20 }}></View>
                            </View>
                            <TouchableOpacity onPress={() => this.cancelSearch()} style={{
                                backgroundColor: "rgb(251,82,87)",
                                position: 'absolute',
                                right: 0,
                                bottom: 0,
                                width: screenWidth - 99,
                                // flex:1,
                                justifyContent: 'center',
                                alignItems: 'center',
                                height: 40,
                                borderBottomLeftRadius: 20,
                                borderBottomRightRadius: 20,
                            }}>
                                <Text style={{ fontSize: 18, color: 'white', fontWeight: 'bold' }}>Cancel</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </Modal>

                {/* Modal for ride completed */}
                <Modal
                    animationType="fade"
                    transparent={true}
                    visible={this.state.showRideComplete}
                // visible={true}
                >
                    <View style={styles.modalContainer}>
                        <View style={styles.rideCompleteModalContent}>
                            <Text style={{ fontSize: 18, fontWeight: 'bold', color: 'rgb(251,82,87)', position: 'absolute', top: 20 }}>You have reach your destination!</Text>
                            <Image source={require('../../assets/tick_greenBg.png')} style={{ width: 50, height: 50 }}></Image>
                            <TouchableOpacity onPress={() => {
                                this.setState({ showRideComplete: false })
                                this.props.navigation.navigate('Home')
                            }} style={{
                                backgroundColor: "rgb(251,82,87)",
                                position: 'absolute',
                                right: 0,
                                bottom: 0,
                                width: screenWidth - 99,
                                // flex:1,
                                justifyContent: 'center',
                                alignItems: 'center',
                                height: 40,
                                borderBottomLeftRadius: 20,
                                borderBottomRightRadius: 20,
                            }}>
                                <Text style={{ fontSize: 18, color: 'white', fontWeight: 'bold' }}>Okay</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </Modal>

                {/* <View style={styles.mapContainer}></View> */}

                <MapView
                    style={styles.mapContainer}
                    initialRegion={{
                        latitude: this.state.pickupMarker.latitude,
                        longitude: this.state.pickupMarker.longitude,
                        latitudeDelta: 0.007,
                        longitudeDelta: 0.007,
                    }}
                    ref={map => { this.map = map }}
                >
                    <Marker
                        coordinate={{ latitude: this.state.pickupMarker.latitude, longitude: this.state.pickupMarker.longitude }}
                        image={require('../../assets/destination_marker.png')}
                        style={{ height: 48, width: 48 }}
                    />
                    <Marker
                        coordinate={{ latitude: this.state.destinationMarker.latitude, longitude: this.state.destinationMarker.longitude }}
                        image={require('../../assets/destination_marker.png')}
                        style={{ height: 48, width: 48 }}
                    />
                    {renderDirection()}

                </MapView>
                {bottomContainer()}

            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    mapContainer: {
        width: screenWidth,
        height: screenHeight - 250,
        // left: 0,
        // right: 0,
        // top: 0,
        // bottom: 0,
        // position: 'absolute',
    },
    bookInfoContainer: {
        // position:'absolute',
        // bottom:0,
        width: screenWidth,
        height: 250,
        backgroundColor: 'white',
        padding: 20,
        position: 'relative'
    },
    modalContainer: {
        backgroundColor: 'rgba(0,0,0,0.3)',
        alignItems: "center",
        justifyContent: "center",
        flex: 1,
    },
    modalContent: {
        width: screenWidth - 100,
        height: 300,
        backgroundColor: 'white',
        borderRadius: 20,
        padding: 20,
    },
    buttonPickRide: {
        height: 80,
        padding: 10,
        width: screenWidth - 30,
        backgroundColor: 'rgba(251,82,87,0.1)',
        borderColor: 'rgb(251,82,87)',
        borderWidth: 2,
        borderStyle: 'solid',
        borderRadius: 20,
        flexDirection: 'row',
        alignItems: 'center'
    },
    selectRideContainer: {
        height: 80,
        padding: 10,
        width: screenWidth - 150,
        backgroundColor: 'rgba(251,82,87,0.1)',
        marginTop: 20,
        borderColor: 'rgb(251,82,87)',
        borderWidth: 2,
        borderStyle: 'solid',
        borderRadius: 20,
        flexDirection: 'row',
        alignItems: 'center'
    },
    questionModalContent: {
        width: screenWidth - 100,
        height: 300,
        backgroundColor: 'white',
        borderRadius: 20,
        padding: 20,
    },
    questionConfirmButton: {
        backgroundColor: 'rgb(251,82,87)',
        width: screenWidth - 130,
        borderRadius: 20,
        height: 35,
        justifyContent: 'center',
        alignItems: 'center',
    },
    confirmBookButton: {
        width: screenWidth - 30,
        height: 40,
        backgroundColor: 'rgb(251,82,87)',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 20,
    },
    radioContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        marginRight: 40,
    },
    genderContainer: {
        width: screenWidth - 130,
        // backgroundColor:'green',
        height: 50,
        borderRadius: 20,
        flexDirection: 'row',
        alignItems: 'center',
    },
    circle: {
        height: 20,
        width: 20,
        borderRadius: 10,
        borderWidth: 1,
        borderColor: '#fb5b57',
        alignItems: 'center',
        justifyContent: 'center',
    },
    checkedCircle: {
        width: 14,
        height: 14,
        borderRadius: 7,
        backgroundColor: '#fb5b57',
    },
    spinnerContainer: {
        backgroundColor: 'rgba(0,0,0,0.3)',
        alignItems: "center",
        justifyContent: "center",
        flex: 1,
        flexDirection: 'column',
    },
    spinnerModalContent: {
        alignItems: "center",
        justifyContent: "center",
        width: 200,
        height: 120,
        backgroundColor: 'white',
        borderRadius: 20,
        position: 'relative'
    },
    offerModalContent: {
        width: screenWidth - 100,
        height: 400,
        backgroundColor: 'white',
        borderRadius: 20,
        position: 'relative',
        padding: 10,
    },
    rideCompleteModalContent: {
        width: screenWidth - 100,
        height: 200,
        backgroundColor: 'white',
        borderRadius: 20,
        position: 'relative',
        padding: 10,
        justifyContent: 'center',
        alignItems: 'center',
    }
})