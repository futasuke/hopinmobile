import React from 'react';
import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
    View,
    Text,
    StatusBar,
    ImageBackground,
    Dimensions,
    Image,
    TouchableOpacity,
    Modal,
} from 'react-native';

import {
    Input,
    Icon,
    Item,
    Label,
    Left,
} from 'native-base';
import 'react-native-gesture-handler';
import axios from 'axios';
import FastImage from 'react-native-fast-image';

const screenWidth = Dimensions.get("window").width;
const screenHeight = Dimensions.get("window").height;

export default class EnterDestination extends React.Component {
    state = {
        currentPlaceName: "",
        destination: '',
        result: [],
        ip: this.props.route.params.ip,
        pickupMarker: this.props.route.params.pickupMarker,
        destinationMarker: [],
        userId: this.props.route.params.userId,
        destinationId: '',
    }

    searchDestination() {
        if (this.state.destination == '') {
            return;
        }

        var self = this;

        console.log(this.state.destination);

        axios.post(this.state.ip + 'api/try-getLocationCache', {
            destination: this.state.destination,
        }).then(function (respond) {
            console.log(respond);
            if (respond.data[0].message == 'Success') {
                self.setState({ result: respond.data[0].content });
                self.testConsole();
            }
            if (respond.data[0].message == 'GetGoogle') {
                console.log('running google api');
                self.getFromGoogle();
            }
        }).catch(function (error) {
            console.log(error);
        })
    }

    getFromGoogle() {
        var self = this;
        var apiKey = 'AIzaSyDtafZr2PyN-K1fg-zBwsvqS-W2Cqz1IuI';
        var dest = this.state.destination;
        // var url = 'https://maps.googleapis.com/maps/api/place/findplacefromtext/json?input='+dest+'&inputtype=textquery&fields=formatted_address,name,geometry&key='+apiKey;
        // var url2 = 'https://maps.googleapis.com/maps/api/place/autocomplete/json?input='+dest+'&types=address&key='+apiKey+'&components=country:my&types=address';
        var url3 = 'https://maps.googleapis.com/maps/api/place/queryautocomplete/json?input=' + dest + '&types=address&key=' + apiKey + '&componentRestrictions=country:my';
        axios.get(url3).then(function (respond) {
            self.setState({ result: [] });
            console.log(respond.data.predictions);
            var temp = self.state.result;
            var jsonTemp = {};
            respond.data.predictions.forEach(element => {
                jsonTemp = {
                    location_id: element.place_id,
                    description: element.description,
                    main_text: element.structured_formatting.main_text,
                    secondary_text: element.structured_formatting.secondary_text,
                }
                temp.push(jsonTemp);
            });
            console.log('temp->');
            console.log(temp);
            self.setState({ result: temp });
            self.storeDestination(respond.data.predictions);
            // var arr = [];
            // arr = respond.data.predictions;
            // for(var i = 0; i<arr.length;i++){
            //     console.log(arr[i].description)
            // }
        }).catch(function (error) {
            console.log(error);
        })
    }

    storeDestination(googleRespond) {
        var self = this;
        axios.post(this.state.ip + 'api/try-storeLocationCache', {
            result: googleRespond,
        }).then(function (respond) {
            console.log(respond);
        }).catch(function (error) {
            console.log(error);
        })
    }

    getLocationCoord(data){
        var self = this;
        // var apiKey = 'AIzaSyDtafZr2PyN-K1fg-zBwsvqS-W2Cqz1IuI';
        var placeId = data.location_id;
        this.setState({destinationId:placeId});
        axios.post(this.state.ip+'api/try-getLocationCoord',{
            location_id: placeId,
        }).then(function(respond){
            console.log(respond);
            if(respond.data[0].message=='Success'){
                console.log('Go to booking map');
                console.log(respond.data[0].content);
                var temp = {
                    latitude : respond.data[0].content.latitude,
                    longitude : respond.data[0].content.longitude,
                }
                self.setState({destinationMarker:temp});
                self.goToBookMap();
            }
            if(respond.data[0].message=='GetGoogle'){
                self.getLocationCoordGoogle(data);
            }
        }).catch(function(error){
            console.log(error);
        });
        // var url = 'https://maps.googleapis.com/maps/api/place/details/json?place_id='+placeId+'&key='+apiKey+'&fields=geometry';
        // axios.get(url).then(function(respond){
        //     console.log(respond);
        //     console.log(respond.data.result.geometry.location.lat);
        //     self.storeLocationCoord(placeId,respond);
        // }).catch(function(error){
        //     console.log(error);
        // });
    }

    getLocationCoordGoogle(data){
        var self = this;
        var apiKey = 'AIzaSyDtafZr2PyN-K1fg-zBwsvqS-W2Cqz1IuI';
        var placeId = data.location_id;
        this.setState({destinationId:placeId});
        var url = 'https://maps.googleapis.com/maps/api/place/details/json?place_id='+placeId+'&key='+apiKey+'&fields=geometry';
        // console.log(placeId);
        // console.log(apiKey);
        axios.get(url).then(function(respond){
            console.log(respond);
            console.log(respond.data.result.geometry.location.lat);
            var temp = {
                latitude : respond.data.result.geometry.location.lat,
                longitude : respond.data.result.geometry.location.lng,
            }
            self.setState({destinationMarker:temp});
            self.storeLocationCoord(placeId,respond);
        }).catch(function(error){
            console.log(error);
        });
    }

    storeLocationCoord(placeId,respond){
        var self = this;

        axios.post(this.state.ip+'api/try-storeLocationCoord',{
            location_id: placeId,
            latitude:respond.data.result.geometry.location.lat,
            longitude:respond.data.result.geometry.location.lng,
        }).then(function(respond){
            console.log(respond);
            self.goToBookMap();
        }).catch(function(error){
            console.log(error);
        });
    }

    goToBookMap(){
        var tempLat = this.state.destinationMarker.latitude;
        var tempLng = this.state.destinationMarker.longitude;
        var tempData = this.state.destinationMarker;
        if(typeof tempLat != 'number' || typeof tempLng != 'number'){
            var tempLat = parseFloat(this.state.destinationMarker.latitude);
            var tempLng = parseFloat(this.state.destinationMarker.longitude);
            tempData = {
                latitude: tempLat,
                longitude: tempLng,
            }
            console.log(tempData);
        }

        this.props.navigation.navigate('BookMainMap', {
            ip:this.state.ip,
            pickupMarker:this.state.pickupMarker,
            destinationMarker:tempData,
            userId: this.state.userId,
            destinationId:this.state.destinationId,
        });
    }

    testConsole() {
        console.log(this.state.result);
    }

    componentDidMount() {
        // console.log(this.state.result);
        // this.inputDirection.focus();
        console.log(this.state.ip);
        console.log('EnterDestination props');
        console.log(this.props);
        // console.log(this.state.pickupMarker);
    }

    render() {
        let searchResult;
        var self = this;
        if (!this.state.result.length) { //if array empty. length 0. empty.
            searchResult = <View></View>
        } else {
            searchResult = this.state.result.map(function (data, i) {
                return (
                    <View key={i} style={styles.resultContainer}>
                        <TouchableOpacity style={{marginBottom:10}} onPress={()=>self.getLocationCoord(data)}>
                            <Text style={{fontSize:18,fontWeight:'bold'}}>{data.main_text}</Text>
                            <Text>{data.secondary_text}</Text>
                        </TouchableOpacity>
                    </View>
                )
            })
        }

        return (
            <View style={styles.container}>
                {/* Back button */}
                <TouchableOpacity style={styles.backButtonContainer} onPress={() => this.props.navigation.goBack()}>
                    <Image source={require('../assets/chevron_left_colored.png')} style={styles.backButton}></Image>
                </TouchableOpacity>

                {/* Enter location components */}
                <View style={styles.enterLocationContainer}>
                    <View>
                        <Image source={require('../assets/destination_panel.png')} style={{ width: 60, height: 120, marginTop: 5 }}></Image>
                    </View>
                    <View style={{ marginTop: 15 }}>
                        <View style={{ width: screenWidth - 150, marginBottom: 10, paddingLeft: 10 }}>
                            <Text ellipsizeMode="tail" numberOfLines={1}>{this.state.currentPlaceName}</Text>
                        </View>
                        <View style={styles.containerSeparator} />
                        <View style={{ width: screenWidth - 150, marginTop: 10 }}>
                            <Item style={styles.destinationButton} rounded>
                                <Input
                                    style={{ fontWeight: 'bold', color: 'rgb(114, 114, 114)' }}
                                    placeholder="Enter destination"
                                    placeholderTextColor="grey"
                                    onChangeText={(destination) => this.setState({ destination })} 
                                    // ref={(input)=>{this.inputDirection = input}}
                                    autoFocus={true}/>
                                <TouchableOpacity style={styles.iconContainer} onPress={() => this.searchDestination()}>
                                    <Image source={require('../assets/search-location.png')} style={styles.inputIcon}></Image>
                                </TouchableOpacity>
                            </Item>
                        </View>
                    </View>
                </View>

                {/* Divider */}
                <View style={{ backgroundColor: 'rgba(150, 150, 150, 0.4)', width: screenWidth, height: 10, marginBottom: 20 }}></View>

                {/* Destination result */}
                {searchResult}
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "white",
    },
    backButtonContainer: {
        marginLeft: 20,
        marginTop: 20,
        // borderColor: 'rgb(251,82,87)',
        // borderWidth: 2,
        // borderStyle: "solid",
        width: 50,
        height: 50,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 25,
    },
    backButton: {
        width: 30,
        height: 30,
    },
    driverIconContainer: {
        flex: 1,
        zIndex: 9,
    },
    containerSeparator: {
        borderStyle: 'solid',
        borderWidth: 1,
        borderColor: 'rgb(251,82,87)',
        width: screenWidth - 110,
    },
    destinationButton: {
        borderStyle: 'solid',
        borderWidth: 2,
        borderColor: 'rgba(0,0,0,0.2)',
        borderRadius: 20,
        height: 50,
        justifyContent: 'center',
        paddingLeft: 10,
        width: screenWidth - 110,
    },
    enterLocationContainer: {
        height: 130,
        backgroundColor: 'white',
        flexDirection: 'row',
        marginLeft: 20,
        marginTop: 20,
        width: screenWidth,
    },
    iconContainer: {
        backgroundColor: 'rgb(251,82,87)',
        borderRadius: 20,
        width: 60,
        height: 52,
        alignItems: "center",
        justifyContent: "center",
    },
    inputIcon: {
        width: 25,
        height: 25,
    },
    resultContainer: {
        borderBottomColor: 'rgba(251,82,87,0.6)',
        borderBottomWidth: 2,
        marginBottom: 20,
        width: screenWidth - 40,
        alignSelf: 'center',
    }
})