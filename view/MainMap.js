import React from 'react';
import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
    View,
    Text,
    StatusBar,
    ImageBackground,
    Dimensions,
    Image,
    TouchableOpacity,
    Modal,
} from 'react-native';

import {
    Input,
    Icon,
    Item,
    Label,
    Left,
} from 'native-base';
import 'react-native-gesture-handler';
import axios from 'axios';
import FastImage from 'react-native-fast-image';
import MapView, { Marker, AnimatedRegion } from 'react-native-maps';
import Geocoder from 'react-native-geocoding';
import { setInterval } from 'core-js';

const screenWidth = Dimensions.get("window").width;
const screenHeight = Dimensions.get("window").height;

/*
    Notes!
    - destinationMarker is actually pickupMarker. Changed.
*/

export default class MainMap extends React.Component {
    state = {
        coord: this.props.route.params.coord,
        currentCoord: this.props.route.params.coord,
        ip: this.props.route.params.ip,
        userId: this.props.route.params.userId,
        coordinate: new AnimatedRegion({
            latitude: '',
            longitude: '',
        }),
        pickupMarker: [],
        destinationName: '',
        currentPlaceName: '',
        driverCoord: [],
    }

    recenterMap() {
        const latitude = this.state.currentCoord.latitude;
        const longitude = this.state.currentCoord.longitude;
        this.map.animateToRegion({
            latitude,
            longitude,
            latitudeDelta: 0.007,
            longitudeDelta: 0.007,
        })

    }

    destinationMarker(region) {
        var self = this;
        var lat = region.latitude;
        var lon = region.longitude;
        console.log("lat : " + lat + "  long" + lon);
        var latlng = {
            latitude: lat,
            longitude: lon,
        }
        this.setState({pickupMarker:latlng});
        // this.testConsole();
        // Geocoder.from(lat, lon)
        //     .then(json => {
        //         var addressComponent = json.results[0].formatted_address;
        //         self.setState({ currentPlaceName: addressComponent });
        //     })
        //     .catch(error => console.warn(error));
    }

    testConsole(){
        console.log(this.state.pickupMarker.latitude);
    }

    async updateDriverLocation() {
        this.interval = setInterval(() => this.getDriverLocation(), 3000);
    }

    getDriverLocation() {
        var self = this;
        var driverArray = [];
        axios.post(self.state.ip + '/api/try-getDriverLocation', {
            lati: self.state.currentCoord.latitude,
            long: self.state.currentCoord.longitude,
        }).then(function (response) {
            // response.data.map((drivaq) => {
            //     driverArray.push(drivaq.content);
            // })
            // self.setState({ driverCoord: driverArray }, function () {
            //     console.log(self.state.driverCoord);
            // });
            console.log(response.data[0].content);
            self.setState({driverCoord:response.data[0].content});
            // console.log('array');
            // console.log(driverArray);
        }).catch(function (error) {
            console.log(error);
        });
    }

    stopInterval() {
        clearInterval(this.interval);
    }

    goEnterDestination(){
        clearInterval(this.interval);
        this.props.navigation.navigate('EnterDestination',{
            ip:this.state.ip,
            pickupMarker:this.state.pickupMarker,
            userId: this.state.userId,
        });
    }

    componentDidMount() {
        var self = this;
        this.onFocus = this.props.navigation.addListener('focus', () => {
            self.updateDriverLocation();
        });
        console.log('MainMap props');
        console.log(this.props);
        // Geocoder.init("AIzaSyDtafZr2PyN-K1fg-zBwsvqS-W2Cqz1IuI");
        // Geocoder.from(this.state.currentCoord.latitude, this.state.currentCoord.longitude).then(json => {
        //     self.setState({ currentPlaceName: json.results[0].formatted_address });
        // }).catch(error => console.warn(error));

    }

    componentWillUnmount() {
        var self = this;
        this.onFocus();
        clearInterval(this.interval);
    }

    render() {
        var self = this;
        // const renderDriver = this.state.driverCoord.map((data) => { return (<View key={data.id}><Text>{data.id}</Text></View>) })
        const testRender = this.state.driverCoord.map((driver, index) => {
            var lat = parseFloat(driver.latitude);
            var long = parseFloat(driver.longitude);
            return (
                <Marker
                    key={index}
                    tracksViewChanges={false}
                    image={require('../assets/driver_marker.png')}
                    title={driver.id.toString()}
                    style={{ height: 48, width: 48 }}
                    coordinate={{ latitude: lat, longitude: long }}
                    ref={marker => { this.marker = marker }}>
                </Marker>
                // <View></View>
            )
        })

        return (
            <View style={styles.container}>

                {/* This is menu button */}
                <TouchableOpacity style={styles.menuContainer} onPress={() => this.props.navigation.goBack()}>
                    <Image style={styles.menuButton} source={require('../assets/chevron_left_withgradient.png')} />
                </TouchableOpacity>

                {/* This is center map button */}
                <TouchableOpacity style={styles.gpsContainer} onPress={() => this.recenterMap()}>
                    <Image style={styles.gpsButton} source={require('../assets/gps.png')} />
                </TouchableOpacity>

                {/* This is enter location */}
                <View style={styles.enterLocationContainer}>
                    <View>
                        <Image source={require('../assets/destination_panel.png')} style={{ width: 60, height: 120, marginTop: 5 }}></Image>
                    </View>
                    <View style={{ marginTop: 15 }}>
                        <View style={{ width: screenWidth - 150, marginBottom: 10, paddingLeft: 10 }}>
                            <Text ellipsizeMode="tail" numberOfLines={1}>{this.state.currentPlaceName}</Text>
                        </View>
                        <View style={styles.containerSeparator} />
                        <View style={{ width: screenWidth - 150, marginTop: 10 }}>
                            <TouchableOpacity style={styles.destinationButton} onPress={()=>this.goEnterDestination()}>
                                <Text style={{fontWeight:'bold',color:'rgb(114, 114, 114)'}}>Enter destination</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>

                {/* This is map */}
                <MapView
                    style={styles.mapContainer}
                    initialRegion={{
                        latitude: this.state.coord.latitude,
                        longitude: this.state.coord.longitude,
                        latitudeDelta: 0.007,
                        longitudeDelta: 0.007,
                    }}
                    ref={map => { this.map = map }}
                    showsUserLocation={true}
                    onRegionChangeComplete={(region) => this.destinationMarker(region)}
                >
                    {testRender}
                    {/* {this.state.driverCoord.map((driver) => (
                        <Marker
                            image={require('../assets/destination_marker.png')}
                            style={{ height: 100, width: 100 }}
                            coordinate={{ latitude: parseFloat(driver.latitude), longitude: parseFloat(driver.longitude) }}>
                        </Marker>
                    ))} */}
                </MapView>
                <View style={styles.markerFixed}>
                    <Image style={styles.marker} source={require('../assets/destination_marker.png')} />
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    mapContainer: {
        left: 0,
        right: 0,
        top: 0,
        bottom: 0,
        position: 'absolute',
    },
    menuContainer: {
        zIndex: 9,
        position: 'absolute',
        top: 40,
        left: 20,
    },
    menuButton: {
        width: 50,
        height: 50,
        borderRadius: 25,
        borderColor: 'white',
        borderWidth: 2,
    },
    gpsContainer: {
        zIndex: 9,
        position: 'absolute',
        bottom: 200,
        right: 20,
    },
    gpsButton: {
        width: 50,
        height: 50,
        borderRadius: 25,
        borderColor: 'white',
        borderWidth: 2,
    },
    enterLocationContainer: {
        zIndex: 9,
        position: 'absolute',
        left: 30,
        bottom: 50,
        width: screenWidth - 60,
        height: 130,
        backgroundColor: 'white',
        flexDirection: 'row',
        borderRadius: 15,
        borderColor: 'rgba(251,82,87,0.3)',
        borderWidth: 2,
        borderStyle: "solid",
    },
    markerFixed: {
        left: '50%',
        marginLeft: -24,
        marginTop: -48,
        position: 'absolute',
        top: '50%'
    },
    marker: {
        height: 48,
        width: 48
    },
    driverIconContainer: {
        flex: 1,
        zIndex: 9,
    },
    containerSeparator: {
        borderStyle: 'solid',
        borderWidth: 1,
        borderColor: 'rgb(251,82,87)',
    },
    destinationButton: {
        borderStyle: 'solid',
        borderWidth: 2,
        borderColor: 'rgba(0,0,0,0.2)',
        borderRadius: 20,
        height: 50,
        justifyContent: 'center',
        paddingLeft: 10,
    }
})