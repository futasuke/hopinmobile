import React from 'react';
import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
    View,
    Text,
    StatusBar,
    ImageBackground,
    Dimensions,
    Image,
    TouchableOpacity,
    Modal,
    ActivityIndicator,
    FlatList,
    RefreshControl,
} from 'react-native';

import {
    Input,
    Icon,
    Item,
    Label,
    Left,
} from 'native-base';
import 'react-native-gesture-handler';
import axios from 'axios';
import FastImage from 'react-native-fast-image';
import MapView, { Marker, AnimatedRegion } from 'react-native-maps';
import MapViewDirections from 'react-native-maps-directions';
import { showLocation } from 'react-native-map-link';

const screenWidth = Dimensions.get("window").width;
const screenHeight = Dimensions.get("window").height;

export default class HikerMainMap extends React.Component {
    state = {
        bottomContent: 'destination_otw',
    }

    render() {
        const bottomContainer = () => {
            if (this.state.bottomContent == 'driver_otw') {
                return (
                    <View style={styles.bottomContainer}>
                        <View style={{ justifyContent: 'center', alignItems: 'center', position: 'absolute', top: 10, alignSelf: 'center' }}>
                            <Text style={{ fontSize: 18, fontWeight: 'bold', }}>Driver is on the way to you. Please wait.</Text>
                        </View>
                        <View style={{ marginTop: 10, alignItems: 'center'}}>
                            <ActivityIndicator size="large" animating={true} color="rgb(251,82,87)" style={{ marginBottom: 15 }} />
                        </View>
                        <TouchableOpacity style={{
                            width: screenWidth - 30,
                            height: 40,
                            backgroundColor: 'rgb(251,82,87)',
                            justifyContent: 'center',
                            alignItems: 'center',
                            borderRadius: 20,
                            position: 'absolute',
                            alignSelf: 'center',
                            bottom: 10,
                        }}>
                            <Text style={{ color: 'white', fontSize: 18, fontWeight: 'bold' }}>Cancel Ride</Text>
                        </TouchableOpacity>
                    </View>
                )
            }
            if (this.state.bottomContent == 'destination_otw') {
                return (
                    <View style={styles.bottomContainer}>
                        <View style={{ justifyContent: 'center', alignItems: 'center', position: 'absolute', top: 10, alignSelf: 'center' }}>
                            <Text style={{ fontSize: 18, fontWeight: 'bold', }}>You are on the way to your destination</Text>
                        </View>
                        <View style={{ marginTop: 10, alignItems: 'center'}}>
                            <ActivityIndicator size="large" animating={true} color="rgb(251,82,87)" style={{ marginBottom: 15 }} />
                        </View>
                        <TouchableOpacity style={{
                            width: screenWidth - 30,
                            height: 40,
                            backgroundColor: 'rgb(251,82,87)',
                            justifyContent: 'center',
                            alignItems: 'center',
                            borderRadius: 20,
                            position: 'absolute',
                            alignSelf: 'center',
                            bottom: 10,
                        }}>
                            <Text style={{ color: 'white', fontSize: 18, fontWeight: 'bold' }}>Show in Waze</Text>
                        </TouchableOpacity>
                    </View>
                )
            }
        }

        return (
            <View style={styles.container}>
                <MapView
                    style={styles.mapContainer}
                    // initialRegion={{
                    //     latitude: this.state.pickupMarker.latitude,
                    //     longitude: this.state.pickupMarker.longitude,
                    //     latitudeDelta: 0.007,
                    //     longitudeDelta: 0.007,
                    // }}
                    ref={map => { this.map = map }}
                >
                </MapView>
                {bottomContainer()}

            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    mapContainer: {
        width: screenWidth,
        height: screenHeight - 250,
        // left: 0,
        // right: 0,
        // top: 0,
        // bottom: 0,
        // position: 'absolute',
    },
    bottomContainer: {
        width: screenWidth,
        height: 250,
        backgroundColor: 'white',
        padding: 20,
        position: 'relative',
        justifyContent:'center',
    },
})