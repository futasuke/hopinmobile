import React from 'react';
import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
    View,
    Text,
    StatusBar,
    ImageBackground,
    Dimensions,
    Image,
    TouchableOpacity,
    PermissionsAndroid,
    FlatList,
    Modal,
    ActivityIndicator,
} from 'react-native';

import {
    Input,
    Icon,
    Item,
    Label,
    Left,
} from 'native-base';
import 'react-native-gesture-handler';
import axios from 'axios';

const screenWidth = Dimensions.get("window").width;
const screenHeight = Dimensions.get("window").height;

export default class EnterDestination extends React.Component {
    state = {
        destination: '',
        coord: this.props.route.params.coord,
        currentCoord: this.props.route.params.coord,
        ip: this.props.route.params.ip,
        userId: this.props.route.params.userId,
        result: [],
        destinationMarker: [],
        destinationId: '',
        showSearchingModal: false,
        routeDetail: '',
        showFoundMatchModal: false,
        searchCarpoolInterval : false,
        matchedRoute: '',
        driverId: '',
        requestDetail: '',
        checkStatusInterval: false,
        driverData: '',
    }

    componentDidMount(){
        this.interval = setInterval(() => this.intervalFunction(), 2000);
    }
    componentWillUnmount(){
        clearInterval(this.interval);
    }

    intervalFunction(){
        if(this.state.searchCarpoolInterval == true){
            this.searchCarpool();
        }
        if(this.state.checkStatusInterval == true){
            this.checkRiderAcceptance();
        }
    }

    searchDestination() {
        if (this.state.destination == '') {
            return;
        }

        var self = this;

        console.log(this.state.destination);

        axios.post(this.state.ip + 'api/try-getLocationCache', {
            destination: this.state.destination,
        }).then(function (respond) {
            console.log(respond);
            if (respond.data[0].message == 'Success') {
                self.setState({ result: respond.data[0].content });
                self.testConsole();
            }
            if (respond.data[0].message == 'GetGoogle') {
                console.log('running google api');
                self.getFromGoogle();
            }
        }).catch(function (error) {
            console.log(error);
        })
    }

    testConsole() {
        console.log(this.state.result);
    }

    getFromGoogle() {
        var self = this;
        var apiKey = 'AIzaSyDtafZr2PyN-K1fg-zBwsvqS-W2Cqz1IuI';
        var dest = this.state.destination;
        // var url = 'https://maps.googleapis.com/maps/api/place/findplacefromtext/json?input='+dest+'&inputtype=textquery&fields=formatted_address,name,geometry&key='+apiKey;
        // var url2 = 'https://maps.googleapis.com/maps/api/place/autocomplete/json?input='+dest+'&types=address&key='+apiKey+'&components=country:my&types=address';
        var url3 = 'https://maps.googleapis.com/maps/api/place/queryautocomplete/json?input=' + dest + '&types=address&key=' + apiKey + '&componentRestrictions=country:my';
        axios.get(url3).then(function (respond) {
            self.setState({ result: [] });
            console.log(respond.data.predictions);
            var temp = self.state.result;
            var jsonTemp = {};
            respond.data.predictions.forEach(element => {
                jsonTemp = {
                    location_id: element.place_id,
                    description: element.description,
                    main_text: element.structured_formatting.main_text,
                    secondary_text: element.structured_formatting.secondary_text,
                }
                temp.push(jsonTemp);
            });
            console.log('temp->');
            console.log(temp);
            self.setState({ result: temp });
            self.storeDestination(respond.data.predictions);
            // var arr = [];
            // arr = respond.data.predictions;
            // for(var i = 0; i<arr.length;i++){
            //     console.log(arr[i].description)
            // }
        }).catch(function (error) {
            console.log(error);
        })
    }

    storeDestination(googleRespond) {
        var self = this;
        axios.post(this.state.ip + 'api/try-storeLocationCache', {
            result: googleRespond,
        }).then(function (respond) {
            console.log(respond);
        }).catch(function (error) {
            console.log(error);
        })
    }

    getLocationCoord(data) {
        var self = this;
        // var apiKey = 'AIzaSyDtafZr2PyN-K1fg-zBwsvqS-W2Cqz1IuI';
        var placeId = data.location_id;
        this.setState({ destinationId: placeId });
        axios.post(this.state.ip + 'api/try-getLocationCoord', {
            location_id: placeId,
        }).then(function (respond) {
            console.log(respond);
            if (respond.data[0].message == 'Success') {
                console.log('Go to booking map');
                console.log(respond.data[0].content);
                var temp = {
                    latitude: respond.data[0].content.latitude,
                    longitude: respond.data[0].content.longitude,
                }
                self.setState({ destinationMarker: temp });
                // self.goToBookMap();
            }
            if (respond.data[0].message == 'GetGoogle') {
                self.getLocationCoordGoogle(data);
            }
        }).catch(function (error) {
            console.log(error);
        });
        // var url = 'https://maps.googleapis.com/maps/api/place/details/json?place_id='+placeId+'&key='+apiKey+'&fields=geometry';
        // axios.get(url).then(function(respond){
        //     console.log(respond);
        //     console.log(respond.data.result.geometry.location.lat);
        //     self.storeLocationCoord(placeId,respond);
        // }).catch(function(error){
        //     console.log(error);
        // });
    }

    getLocationCoordGoogle(data) {
        var self = this;
        var apiKey = 'AIzaSyDtafZr2PyN-K1fg-zBwsvqS-W2Cqz1IuI';
        var placeId = data.location_id;
        this.setState({ destinationId: placeId });
        var url = 'https://maps.googleapis.com/maps/api/place/details/json?place_id=' + placeId + '&key=' + apiKey + '&fields=geometry';
        // console.log(placeId);
        // console.log(apiKey);
        axios.get(url).then(function (respond) {
            console.log(respond);
            console.log(respond.data.result.geometry.location.lat);
            var temp = {
                latitude: respond.data.result.geometry.location.lat,
                longitude: respond.data.result.geometry.location.lng,
            }
            self.setState({ destinationMarker: temp });
            self.storeLocationCoord(placeId, respond);
        }).catch(function (error) {
            console.log(error);
        });
    }

    storeLocationCoord(placeId, respond) {
        var self = this;

        axios.post(this.state.ip + 'api/try-storeLocationCoord', {
            location_id: placeId,
            latitude: respond.data.result.geometry.location.lat,
            longitude: respond.data.result.geometry.location.lng,
        }).then(function (respond) {
            console.log(respond);
            // self.goToBookMap();
        }).catch(function (error) {
            console.log(error);
        });
    }

    saveRoute(data){
        var self = this;
        console.log('search carpool');
        console.log(data);
        console.log(this.state.userId);
        console.log(this.state.currentCoord);
        this.setState({showSearchingModal: true, destinationId: data.location_id});
        axios.post(this.state.ip+'api/hiker-saveHikerRoute',{
            hikerId: this.state.userId,
            locationId: data.location_id,
            latitude: this.state.currentCoord.latitude,
            longitude: this.state.currentCoord.longitude,
        }).then(function(respond){
            console.log('saving route');
            console.log(respond);
            console.log('new route');
            console.log(respond.data[0].newRoute);
            console.log('new request');
            console.log(respond.data[0].newRequest);
            self.setState({routeDetail:respond.data[0].newRoute, searchCarpoolInterval: true, requestDetail:respond.data[0].newRequest});
        }).catch(function(error){
            console.log(error);
        })
    }

    searchCarpool(){ //this is interval
        var self = this;
        console.log(this.state.destinationId);
        axios.post(this.state.ip+'api/hiker-searchCarpool',{
            hikerId: this.state.userId,
            routeId: this.state.routeDetail.id,
            destinationId: this.state.destinationId,
        }).then(function(respond){
            console.log(respond);
            if(respond.data[0].message=='Found'){
                self.setState({
                    searchCarpoolInterval:false, 
                    showSearchingModal:false, 
                    showFoundMatchModal:true, 
                    driverId: respond.data[0].content.driver_id,
                    matchedRoute: respond.data,
                    checkStatusInterval:true,
                });
            }
        }).catch(function(error){
            console.log(error);
        });
    }

    checkRiderAcceptance(){ // this is interval after search carpool is done
        var self = this; 
        axios.post(this.state.ip+'api/hiker-hikerCheckStatus',{
            requestId: this.state.requestDetail.id,
        }).then(function(respond){
            console.log('check status');
            console.log(respond);
            if(respond.data[0].message == 'Accepted'){
                self.setState({driverData:respond.data[0].content, checkStatusInterval:false, showFoundMatchModal:false});
            }
            // Save driver to state.
            // Go to view map;
        }).catch(function(error){
            console.log(error);
        });
    }

    cancelSearch(){
        console.log('cancel search');
        this.setState({showSearchingModal: false});
    }

    goToMap(){
        /*
            we send driverId
            we send destination coords
            we send currentCoords
        */
    }

    render() {
        let searchResult;
        var self = this;
        if (!this.state.result.length) { //if array empty. length 0. empty.
            searchResult = <View></View>
        } else {
            searchResult = this.state.result.map(function (data, i) {
                return (
                    <View key={i} style={styles.resultContainer}>
                        {/* <TouchableOpacity style={{ marginBottom: 10 }} onPress={() => self.getLocationCoord(data)}> */}
                        <TouchableOpacity style={{ marginBottom: 10 }} onPress={() => self.saveRoute(data)}>
                            <Text style={{ fontSize: 18, fontWeight: 'bold' }}>{data.main_text}</Text>
                            <Text>{data.secondary_text}</Text>
                        </TouchableOpacity>
                    </View>
                )
            })
        }

        return (
            <View style={styles.container}>

                {/* Modal for searching for carpool */}
                <Modal
                    animationType="fade"
                    transparent={true}
                    visible={this.state.showSearchingModal}
                >
                    <View style={styles.modalContainer}>
                        <View style={{
                            width: screenWidth - 100,
                            height: 200,
                            backgroundColor: 'white',
                            borderRadius: 20,
                            padding: 15,
                            justifyContent: 'center',
                            position: 'relative'
                        }}>
                            <View style={{ position: 'absolute', top: 15, left: 15, alignItems: 'center', width: '100%', }}>
                                <Text style={{ color: 'rgb(251,82,87)', fontWeight: 'bold', fontSize: 18 }}>Searching for carpool that has the same route</Text>
                            </View>
                            <View>
                                <ActivityIndicator size="large" animating={true} color="rgb(251,82,87)" style={{ marginBottom: 15 }} />
                            </View>
                            <TouchableOpacity onPress={()=>this.cancelSearch()} style={{
                                width: '100%',
                                height: 40,
                                backgroundColor: 'rgb(251,82,87)',
                                justifyContent: 'center',
                                alignItems: 'center',
                                borderRadius: 20,
                                position: 'absolute',
                                alignSelf: 'center',
                                bottom: 10,
                            }}>
                                <Text style={{color:'white',fontSize:18,fontWeight:'bold'}}>Cancel</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </Modal>

                {/* Modal for found a match. Please wait for rider confirmation. */}
                <Modal
                    animationType="fade"
                    transparent={true}
                    visible={this.state.showFoundMatchModal}
                >
                    <View style={styles.modalContainer}>
                        <View style={{
                            width: screenWidth - 100,
                            height: 200,
                            backgroundColor: 'white',
                            borderRadius: 20,
                            padding: 15,
                            justifyContent: 'center',
                            position: 'relative'
                        }}>
                            <View style={{ position: 'absolute', top: 15, left: 15, alignItems: 'center', width: '100%', }}>
                                <Text style={{ color: 'rgb(251,82,87)', fontWeight: 'bold', fontSize: 18 }}>Found match. Please wait for rider confirmation</Text>
                            </View>
                            <View>
                                <ActivityIndicator size="large" animating={true} color="rgb(251,82,87)" style={{ marginBottom: 15 }} />
                            </View>
                            {/* <TouchableOpacity onPress={()=>this.cancelSearch()} style={{
                                width: '100%',
                                height: 40,
                                backgroundColor: 'rgb(251,82,87)',
                                justifyContent: 'center',
                                alignItems: 'center',
                                borderRadius: 20,
                                position: 'absolute',
                                alignSelf: 'center',
                                bottom: 10,
                            }}>
                                <Text style={{color:'white',fontSize:18,fontWeight:'bold'}}>Cancel</Text>
                            </TouchableOpacity> */}
                        </View>
                    </View>
                </Modal>

                {/* Back button */}
                <TouchableOpacity style={styles.backButtonContainer} onPress={() => this.props.navigation.goBack()}>
                    <Image source={require('../../assets/chevron_left_colored.png')} style={styles.backButton}></Image>
                </TouchableOpacity>

                {/* Enter location components */}
                <View style={styles.enterLocationContainer}>
                    <View>
                        <Image source={require('../../assets/destination_panel.png')} style={{ width: 60, height: 120, marginTop: 5 }}></Image>
                    </View>
                    <View style={{ marginTop: 15 }}>
                        <View style={{ width: screenWidth - 150, marginBottom: 10, paddingLeft: 10 }}>
                            <Text ellipsizeMode="tail" numberOfLines={1}></Text>
                        </View>
                        <View style={styles.containerSeparator} />
                        <View style={{ width: screenWidth - 150, marginTop: 10 }}>
                            <Item style={styles.destinationButton} rounded>
                                <Input
                                    style={{ fontWeight: 'bold', color: 'rgb(114, 114, 114)' }}
                                    placeholder="Enter destination"
                                    placeholderTextColor="grey"
                                    onChangeText={(destination) => this.setState({ destination })}
                                    // ref={(input)=>{this.inputDirection = input}}
                                    autoFocus={true} />
                                <TouchableOpacity style={styles.iconContainer} onPress={() => this.searchDestination()}>
                                    <Image source={require('../../assets/search-location.png')} style={styles.inputIcon}></Image>
                                </TouchableOpacity>
                            </Item>
                        </View>
                    </View>
                </View>

                {/* Divider */}
                <View style={{ backgroundColor: 'rgba(150, 150, 150, 0.4)', width: screenWidth, height: 10, marginBottom: 20 }}></View>

                {/* Destination result */}
                {searchResult}

            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "white",
    },
    backButtonContainer: {
        marginLeft: 20,
        marginTop: 20,
        // borderColor: 'rgb(251,82,87)',
        // borderWidth: 2,
        // borderStyle: "solid",
        width: 50,
        height: 50,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 25,
    },
    backButton: {
        width: 30,
        height: 30,
    },
    driverIconContainer: {
        flex: 1,
        zIndex: 9,
    },
    containerSeparator: {
        borderStyle: 'solid',
        borderWidth: 1,
        borderColor: 'rgb(251,82,87)',
        width: screenWidth - 110,
    },
    destinationButton: {
        borderStyle: 'solid',
        borderWidth: 2,
        borderColor: 'rgba(0,0,0,0.2)',
        borderRadius: 20,
        height: 50,
        justifyContent: 'center',
        paddingLeft: 10,
        width: screenWidth - 110,
    },
    enterLocationContainer: {
        height: 130,
        backgroundColor: 'white',
        flexDirection: 'row',
        marginLeft: 20,
        marginTop: 20,
        width: screenWidth,
    },
    iconContainer: {
        backgroundColor: 'rgb(251,82,87)',
        borderRadius: 20,
        width: 60,
        height: 52,
        alignItems: "center",
        justifyContent: "center",
    },
    inputIcon: {
        width: 25,
        height: 25,
    },
    resultContainer: {
        borderBottomColor: 'rgba(251,82,87,0.6)',
        borderBottomWidth: 2,
        marginBottom: 20,
        width: screenWidth - 40,
        alignSelf: 'center',
    },
    modalContainer: {
        backgroundColor: 'rgba(0,0,0,0.3)',
        alignItems: "center",
        justifyContent: "center",
        flex: 1,
        flexDirection: 'column',
    },
})