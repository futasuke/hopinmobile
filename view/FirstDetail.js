import React from 'react';
import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
    View,
    Text,
    StatusBar,
    ImageBackground,
    Dimensions,
    Image,
    TouchableOpacity,
    Modal,
    ActivityIndicator,
    KeyboardAvoidingView,
} from 'react-native';

import {
    Input,
    Icon,
    Item,
    Label,
    Left,
    H2,
    Radio,
    ListItem,
    Picker,
    Form,
} from 'native-base';
import 'react-native-gesture-handler';
import axios from 'axios';
import { StackActions, NavigationActions, BaseRouter } from '@react-navigation/native';

const screenWidth = Dimensions.get("window").width;
const screenHeight = Dimensions.get("window").height;

export default class FirstDetail extends React.Component {
    state = {
        fname: '',
        lname: '',
        email: '',
        ip: this.props.route.params.ip,
        modalVisible: false,
        alertMsg: '',
        showSpinner: false,
        userId: this.props.route.params.userId,
        radioValue: null,
    }

    checkInput() {
        if (this.state.fname != '') {
            if (this.state.lname != '') {
                if (this.state.email != '') {
                    if(this.state.radioValue != null){
                        this.tryEnterDetail();
                    }else{
                        this.setState({ alertMsg: 'Please select gender' });
                        this.setState({ modalVisible: true });
                    }
                }else{
                    this.setState({ alertMsg: 'Please enter email address' });
                    this.setState({ modalVisible: true });
                }
            }else{
                this.setState({ alertMsg: 'Please enter last name' });
                this.setState({ modalVisible: true });
            }
        }else{
            this.setState({ alertMsg: 'Please enter first name' });
            this.setState({ modalVisible: true });
        }
    }

    tryEnterDetail() {
        var self = this;
        this.setState({ showSpinner: true });

        axios.post(self.state.ip + 'api/try-firstDetail', {
            userId: self.state.userId,
            fname: self.state.fname,
            lname: self.state.lname,
            email: self.state.email,
            gender: self.state.radioValue,
        }).then(function (response) {
            if (response.data[0].message == "Success") {
                console.log(response);
                self.setState({ showSpinner: false });
                self.props.navigation.dispatch(StackActions.replace('HomeDrawer', {
                    userId: self.state.userId,
                    userDetail: response.data[0].content,
                    dpImage: self.state.ip + response.data[0].content.dpPath,
                    ip: self.state.ip,
                }));
            } else {
                self.setState({ showSpinner: false });
                self.setState({ alertMsg: response.data[0].message });
                self.setState({ modalVisible: true });
                console.log(response.data[0].message);
            }
        }).catch(function (error) {
            self.setState({ showSpinner: false });
            console.log(error);
        });
    }

    componentDidMount() {
        console.log(this.state.ip);
    }

    onValueChange(value) {
        this.setState({
            selected: value
        });
    }

    testRadio(){
        console.log(this.state.radioValue);
    }

    render() {
        const gender = [{ key: '1', text: 'Male', value: 'male' }, { key: '2', value: 'female', text: 'Female' }];

        return (

            // <ImageBackground source={require('../assets/loginBg.png')} style={styles.bgimage}>
            <ImageBackground style={styles.bgimage}>
                <ScrollView contentContainerStyle={{ alignItems: 'center' }}>
                    <KeyboardAvoidingView behavior="padding" style={styles.container}>

                        {/* This is for ActivityIndicator aka Spinner */}
                        <Modal
                            animationType="fade"
                            transparent={true}
                            visible={this.state.showSpinner}
                        >
                            <View style={styles.spinnerContainer}>
                                <View style={styles.spinnerModalContent}>
                                    <ActivityIndicator size="large" animating={true} color="rgb(251,82,87)" />
                                </View>
                            </View>
                        </Modal>

                        {/* This is modal for alert */}
                        <Modal
                            animationType="fade"
                            transparent={true}
                            visible={this.state.modalVisible}
                        >
                            <View style={styles.modalContainer}>
                                <View style={styles.modalContent}>
                                    <View
                                        style={{
                                            borderBottomColor: "rgb(251,82,87)",
                                            borderBottomWidth: 2,
                                            borderStyle: 'solid',
                                            marginBottom: 20,
                                        }}
                                    >
                                        <Text
                                            style={{
                                                fontWeight: 'bold',
                                                fontSize: 24,
                                                color: "rgb(251,82,87)",
                                                marginBottom: 5,
                                            }}>ALERT</Text>
                                    </View>

                                    <Text>{this.state.alertMsg}</Text>

                                    <TouchableOpacity
                                        onPress={() => {
                                            this.setState({ modalVisible: false });
                                        }}
                                        style={styles.hideModalButton}>

                                        <Text
                                            style={{
                                                fontWeight: 'bold',
                                                color: 'white',
                                            }}
                                        >Close</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </Modal>

                        <View>
                            <Image source={require('../assets/bannerSmall.png')} style={styles.logoBanner}></Image>
                        </View>

                        {/* This is for title */}
                        <View style={styles.titleContainer}>
                            <Text style={styles.titleLabel}>Enter your first detail</Text>
                        </View>

                        {/* This is for first name */}
                        <Item rounded style={styles.firstNameContainer}>
                            <View style={styles.iconContainer}>
                                <Image source={require('../assets/user.png')} style={styles.inputIcon}></Image>
                            </View>
                            <Input
                                placeholder="First Name"
                                placeholderTextColor="white"
                                style={{ color: 'white' }}
                                onChangeText={(fname) => this.setState({ fname })} />
                        </Item>

                        {/* This is for last name */}
                        <Item rounded style={styles.lastNameContainer}>
                            <View style={styles.iconContainer}>
                                <Image source={require('../assets/user.png')} style={styles.inputIcon}></Image>
                            </View>
                            <Input
                                placeholder="Last Name"
                                placeholderTextColor="white"
                                style={{ color: 'white' }}
                                onChangeText={(lname) => this.setState({ lname })} />
                        </Item>

                        {/* This is for phone */}
                        {/* UPDATE: PHONE IS NOW REPLACED WITH EMAIL */}
                        <Item rounded style={styles.phoneContainer}>
                            <View style={styles.iconContainer}>
                                <Image source={require('../assets/email.png')} style={styles.inputIcon}></Image>
                            </View>
                            <Input
                                placeholder="Email Address"
                                placeholderTextColor="white"
                                style={{ color: 'white' }}
                                keyboardType='email-address'
                                onChangeText={(email) => this.setState({ email })} />
                        </Item>

                        <View style={styles.genderContainer}>
                            <View style={{marginRight:50, marginLeft:25}}>
                                <Text style={{color:'#fb5b57', fontWeight:'bold', fontSize:18}}>Gender</Text>
                            </View>
                            {gender.map(item => {
                                return (
                                    <View key={item.key} style={styles.buttonContainer}>
                                        <Text style={{color:'#fb5b57', fontWeight:'bold', marginRight:15}}>{item.text}</Text>
                                        <TouchableOpacity
                                            style={styles.circle}
                                            onPress={() => this.setState({ radioValue: item.value })}
                                        >
                                            {this.state.radioValue == item.value && (<View style={styles.checkedCircle} />)}
                                        </TouchableOpacity>
                                    </View>
                                )
                            })}
                        </View>




                        {/* This is next button */}
                        <TouchableOpacity style={styles.nextButton} onPress={() => this.checkInput()}>
                            <Text style={styles.buttonLabel}>Next</Text>
                        </TouchableOpacity>

                    </KeyboardAvoidingView>
                </ScrollView>
            </ImageBackground>

        );
    }
}

const styles = StyleSheet.create({
    bgimage: {
        width: screenWidth,
        height: screenHeight,
        backgroundColor: '#fb5b57',
    },
    container: {
        // justifyContent:"center",
        marginTop: 50,
        alignItems: "center",
        flex: 1,
    },
    titleContainer: {
        // borderBottomColor: 'white',
        // borderBottomWidth: 2,
        marginTop:50,
        marginBottom: 50,
    },
    titleLabel: {
        fontSize: 32,
        fontFamily: 'Aria',
        fontWeight: 'bold',
        color: 'white',
    },
    firstNameContainer: {
        width: screenWidth - 100,
        // paddingLeft:20,
        backgroundColor: "rgba(0,0,0,0.2)",
        marginBottom: 30,
    },
    iconContainer: {
        backgroundColor: 'white',
        borderRadius: 30,
        width: 60,
        height: 51,
        alignItems: "center",
        justifyContent: "center",
    },
    inputIcon: {
        width: 33,
        height: 25,
    },
    lastNameContainer: {
        width: screenWidth - 100,
        // paddingLeft:20,
        backgroundColor: "rgba(0,0,0,0.2)",
        marginBottom: 30,
    },
    phoneContainer: {
        width: screenWidth - 100,
        // paddingLeft:20,
        backgroundColor: "rgba(0,0,0,0.2)",
        marginBottom: 30,
    },
    genderContainer:{
        width: screenWidth - 100,
        backgroundColor:'white',
        height:50,
        borderRadius:20,
        flexDirection:'row', 
        alignItems:'center', 
        marginBottom:60,
    },
    nextButton: {
        backgroundColor: 'white',
        width: screenWidth - 100,
        alignItems: "center",
        justifyContent: "center",
        height: 50,
        borderRadius: 30,
        marginBottom: 40,
    },
    buttonLabel: {
        color: "rgb(251,82,87)",
        fontWeight: 'bold',
    },
    modalContainer: {
        backgroundColor: 'rgba(0,0,0,0.3)',
        alignItems: "center",
        justifyContent: "center",
        flex: 1,
        flexDirection: 'column',
    },
    modalContent: {
        width: 300,
        height: 200,
        padding: 10,
        backgroundColor: 'white',
        borderRadius: 20,
    },
    hideModalButton: {
        position: 'absolute',
        bottom: 10,
        right: 20,
        backgroundColor: "rgb(251,82,87)",
        height: 30,
        width: 60,
        borderRadius: 20,
        alignItems: "center",
        justifyContent: "center",
    },
    spinnerContainer: {
        backgroundColor: 'rgba(0,0,0,0.3)',
        alignItems: "center",
        justifyContent: "center",
        flex: 1,
        flexDirection: 'column',
    },
    spinnerModalContent: {
        alignItems: "center",
        justifyContent: "center",
        width: 200,
        height: 100,
        backgroundColor: 'white',
        borderRadius: 20,
    },
    logoBanner: {
        width: 300,
        height: 150,
        borderRadius: 20,
    },
    buttonContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        marginRight: 40,
    },
    circle: {
        height: 20,
        width: 20,
        borderRadius: 10,
        borderWidth: 1,
        borderColor: '#fb5b57',
        alignItems: 'center',
        justifyContent: 'center',
    },
    checkedCircle: {
        width: 14,
        height: 14,
        borderRadius: 7,
        backgroundColor: '#fb5b57',
    },
})