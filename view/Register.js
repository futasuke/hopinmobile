import React from 'react';
import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
    View,
    Text,
    StatusBar,
    ImageBackground,
    Dimensions,
    Image,
    TouchableOpacity,
    Modal,
    ActivityIndicator,
    KeyboardAvoidingView,
} from 'react-native';

import {
    Input,
    Icon,
    Item,
    Label,
    Left,
    H2,
    Radio,
    ListItem,
} from 'native-base';
import 'react-native-gesture-handler';
import axios from 'axios';
import { StackActions, NavigationActions, BaseRouter } from '@react-navigation/native';

const screenWidth = Dimensions.get("window").width;
const screenHeight = Dimensions.get("window").height;

export default class RegisterScreen extends React.Component {
    state = {
        phone: '',
        // password: '',
        // retype: '',
        ip: this.props.route.params.ip,
        modalVisible: false,
        alertMsg: '',
        showSpinner: false,
        userId: '',
        userDetail: [],
    }

    checkInput() {
        if (this.state.phone != '') {
            var str = this.state.phone;

            // check if there is a not-a-number character
            for (var i = 0; i < str.length; i++) {

                if(str.charAt(i).trim().length==0){
                    this.setState({ alertMsg: 'Please make sure there is no space between numbers'});
                    this.setState({ modalVisible: true });
                    return;
                }

                if (isNaN(str.charAt(i)) == true) {
                    // alert('ada benda bukan nombor');
                    this.setState({ alertMsg: 'Please enter numbers only (example : 0142957391)' });
                    this.setState({ modalVisible: true });
                    return;
                }
            }

            // if all is number
            this.tryRegister();
        }
        else {
            this.setState({ alertMsg: 'Please enter phone number' });
            this.setState({ modalVisible: true });
        }
    }

    tryRegister() {
        var self = this;
        console.log(this.state.phone)

        this.setState({ showSpinner: true });

        axios.post(self.state.ip + 'api/try-register', {
            phone: self.state.phone
        }).then(function (response) {
            self.setState({ showSpinner: false });
            console.log(response.data[0].message);
            if(response.data[0].message == "Success"){
                console.log(response)
                self.setState({ userId: response.data[0].content.id })
                self.setState({ userDetail: response.data[0].content })
                    self.props.navigation.dispatch(StackActions.replace('FirstDetail', {
                    userId: self.state.userId,
                    userDetail: self.state.userDetail,
                    ip: self.state.ip,
                }));
                // self.testState();
            }

            /*
                Old stuff. Instead of using password to create account, we only use phone number
            */
           
            // if (response.data[0].message == "Success") {
            //     console.log(response);
            //     self.setState({ userId: response.data[0].content.id })
            //     self.setState({ userDetail: response.data[0].content })
            //     // console.log(self.state.userId);
            //     self.setState({ showSpinner: false });
            //     self.props.navigation.dispatch(StackActions.replace('FirstDetail', {
            //         userId: self.state.userId,
            //         userDetail: self.state.userDetail,
            //         ip: self.state.ip,
            //     }));
            // }
            // else {
            //     self.setState({ showSpinner: false });
            //     console.log(response.data[0].message);
            //     self.setState({ alertMsg: response.data[0].message });
            //     self.setState({ modalVisible: true });
            // }
            
        }).catch(function (error) {
            console.log(error);
            self.setState({ modalVisible: true });
            self.setState({ showSpinner: false });
        });
    }

    testState(){
        console.log(this.state.userId);
        console.log(this.state.userDetail);
    }

    goToLogin() {
        this.props.navigation.navigate('Login');
    }

    testRemove() {
        const resetAction = StackActions.replace({
            // key:'Register',
            newKey: 'FirstDetail',
            routeName: 'FirstDetail',
            // index: 0,
            // action: [NavigationActions.navigate({routeName:'FirstDetail'})],
        });

        this.props.navigation.dispatch(resetAction);
        // this.props.navigation.navigate('FirstDetail');
        // this.props.navigation.immediatelyResetRouteStack([{component:FirstDetail}]);
    }

    render() {
        return (
            <ImageBackground style={styles.bgimage}>
                <ScrollView contentContainerStyle={{ alignItems: "center" }}>
                    <KeyboardAvoidingView behavior="padding" style={styles.container}>

                        {/* This is for ActivityIndicator aka Spinner */}
                        <Modal
                            animationType="fade"
                            transparent={true}
                            visible={this.state.showSpinner}
                        >
                            <View style={styles.spinnerContainer}>
                                <View style={styles.spinnerModalContent}>
                                    <ActivityIndicator size="large" animating={true} color="rgb(251,82,87)" />
                                </View>
                            </View>
                        </Modal>


                        {/* This is modal for alert */}
                        <Modal
                            animationType="fade"
                            transparent={true}
                            visible={this.state.modalVisible}
                        >
                            <View style={styles.modalContainer}>
                                <View style={styles.modalContent}>
                                    <View
                                        style={{
                                            borderBottomColor: "rgb(251,82,87)",
                                            borderBottomWidth: 2,
                                            borderStyle: 'solid',
                                            marginBottom: 20,
                                        }}
                                    >
                                        <Text
                                            style={{
                                                fontWeight: 'bold',
                                                fontSize: 24,
                                                color: "rgb(251,82,87)",
                                                marginBottom: 5,
                                            }}>ALERT</Text>
                                    </View>

                                    <Text>{this.state.alertMsg}</Text>

                                    <TouchableOpacity
                                        onPress={() => {
                                            this.setState({ modalVisible: false });
                                        }}
                                        style={styles.hideModalButton}>

                                        <Text
                                            style={{
                                                fontWeight: 'bold',
                                                color: 'white',
                                            }}
                                        >Close</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </Modal>

                        <View>
                            <Image source={require('../assets/bannerSmall.png')} style={styles.logoBanner}></Image>
                        </View>

                        {/* This is for title */}
                        <View style={styles.titleContainer}>
                            <Text style={styles.titleLabel}>Welcome</Text>
                        </View>
                        <View style={styles.descriptionContainer}>
                            {/* <Text style={styles.descriptionLabel}>Enter your mobile number to continue</Text> */}
                            <Text style={styles.descriptionLabel}>(Malaysia number only)</Text>
                        </View>

                        {/* This is for email */}
                        <Item rounded style={styles.emailContainer}>
                            <View style={styles.iconContainer}>
                                <Image source={require('../assets/malaysia.png')} style={styles.inputIcon}></Image>
                            </View>
                            <Input
                                placeholder="Phone number"
                                placeholderTextColor="white"
                                style={{ color: 'white' }}
                                keyboardType="phone-pad"
                                onChangeText={(phone) => this.setState({ phone })} />
                        </Item>

                        {/* This is for password */}
                        {/* <Item rounded style={styles.passwordContainer}>
                            <View style={styles.iconContainer}>
                                <Image source={require('../assets/password.png')} style={styles.inputIcon}></Image>
                            </View>
                            <Input
                                placeholder="Password"
                                placeholderTextColor="white"
                                style={{ color: 'white' }}
                                secureTextEntry={true}
                                onChangeText={(password) => this.setState({ password })} />
                        </Item> */}

                        {/* This is retype password */}
                        {/* <Item rounded style={styles.repasswordContainer}>
                            <View style={styles.iconContainer}>
                                <Image source={require('../assets/password.png')} style={styles.inputIcon}></Image>
                            </View>
                            <Input
                                placeholder="Retype Password"
                                placeholderTextColor="white"
                                style={{ color: 'white' }}
                                secureTextEntry={true}
                                onChangeText={(retype) => this.setState({ retype })} />
                        </Item> */}

                        {/* This is register button */}
                        <TouchableOpacity style={styles.registerButton} onPress={() => this.checkInput()}>
                            <Text style={styles.buttonLabel}>Register</Text>
                        </TouchableOpacity>

                        {/* This is back button */}
                        <TouchableOpacity style={styles.backButton} onPress={() => this.goToLogin()}>
                            <Text style={styles.backButtonLabel}>Back</Text>
                        </TouchableOpacity>



                    </KeyboardAvoidingView>
                </ScrollView>
                <View style={styles.socialContainer}>
                    <View>
                        <Text style={styles.socialText}>Or continue with a social  account</Text>
                    </View>
                    <View style={styles.socialBox}>
                        <TouchableOpacity>
                            <Image source={require('../assets/facebookBanner.png')} style={styles.facebookBanner}></Image>
                        </TouchableOpacity>
                        <TouchableOpacity style={{ borderRadius:10,borderColor:'black', borderWidth:2, borderStyle:'solid',}}>
                            <Image source={require('../assets/googleBanner.png')} style={styles.googleBanner}></Image>
                        </TouchableOpacity>
                    </View>
                </View>
            </ImageBackground>

        )
    }
}

const styles = StyleSheet.create({
    bgimage: {
        width: screenWidth,
        height: screenHeight,
        backgroundColor: '#fb5b57',
    },
    container: {
        // justifyContent:"center",
        marginTop: 50,
        alignItems: "center",
        flex: 1,
    },
    titleContainer: {
        marginTop: 40,
        // borderBottomColor: 'white',
        // borderBottomWidth: 2,
        // marginBottom: 50,
    },
    titleLabel: {
        fontSize: 32,
        fontFamily: 'Aria',
        fontWeight: 'bold',
        color: 'white',
    },
    descriptionContainer: {
        marginBottom: 70,
        marginTop: 20,
        alignItems: 'center'
    },
    descriptionLabel: {
        color: 'white',
        fontWeight: 'bold',
    },
    emailContainer: {
        width: screenWidth - 100,
        // paddingLeft:20,
        backgroundColor: "rgba(0,0,0,0.2)",
        marginBottom: 80,
    },
    // passwordContainer: {
    //     width: screenWidth - 100,
    //     // paddingLeft:20,
    //     backgroundColor: "rgba(0,0,0,0.2)",
    //     marginBottom: 30,
    // },
    // repasswordContainer: {
    //     width: screenWidth - 100,
    //     // paddingLeft:20,
    //     backgroundColor: "rgba(0,0,0,0.2)",
    //     marginBottom: 60,
    // },
    iconContainer: {
        backgroundColor: 'white',
        borderRadius: 30,
        width: 60,
        height: 51,
        alignItems: "center",
        justifyContent: "center",
    },
    inputIcon: {
        width: 45,
        height: 30,
    },
    registerButton: {
        backgroundColor: 'white',
        width: screenWidth - 100,
        alignItems: "center",
        justifyContent: "center",
        height: 50,
        borderRadius: 30,
        marginBottom: 40,
    },
    buttonLabel: {
        color: "rgb(251,82,87)",
        fontWeight: 'bold',
    },
    backButton: {
        // backgroundColor: "#fb5b57",
        // width: screenWidth - 100,
        alignItems: "center",
        justifyContent: "center",
        height: 50,
        borderRadius: 30,
        marginBottom: 60,
    },
    backButtonLabel: {
        color: "white",
        fontWeight: 'bold',
    },
    modalContainer: {
        backgroundColor: 'rgba(0,0,0,0.3)',
        alignItems: "center",
        justifyContent: "center",
        flex: 1,
        flexDirection: 'column',
    },
    modalContent: {
        width: 300,
        height: 200,
        padding: 10,
        backgroundColor: 'white',
        borderRadius: 20,
    },
    hideModalButton: {
        position: 'absolute',
        bottom: 10,
        right: 20,
        backgroundColor: "rgb(251,82,87)",
        height: 30,
        width: 60,
        borderRadius: 20,
        alignItems: "center",
        justifyContent: "center",
    },
    spinnerContainer: {
        backgroundColor: 'rgba(0,0,0,0.3)',
        alignItems: "center",
        justifyContent: "center",
        flex: 1,
        flexDirection: 'column',
    },
    spinnerModalContent: {
        alignItems: "center",
        justifyContent: "center",
        width: 200,
        height: 100,
        backgroundColor: 'white',
        borderRadius: 20,
    },
    logoBanner: {
        width: 300,
        height: 150,
        borderRadius: 20,
    },
    socialContainer: { 
        position: 'absolute',
        bottom: 0,
        alignItems: 'center'
    },
    socialText:{
        color:'white',
        fontWeight:'bold',
        marginBottom:25,
    },
    socialBox:{
        backgroundColor: 'white',
        width: screenWidth,
        height: 140,
        alignItems:'center',
        justifyContent:'center',
        flexDirection:'row',
    },
    facebookBanner:{
        width:145,
        height:50,
        borderRadius:10,
        marginRight:50,
    },
    googleBanner:{
        width:145,
        height:50,
    },
})