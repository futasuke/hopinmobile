import React from 'react';
import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
    View,
    Text,
    StatusBar,
    ImageBackground,
    Dimensions,
    Image,
    TouchableOpacity,
    PermissionsAndroid,
    Linking,
} from 'react-native';

import {
    Input,
    Icon,
    Item,
    Label,
    Left,
} from 'native-base';
import 'react-native-gesture-handler';
import FastImage from 'react-native-fast-image';

const screenWidth = Dimensions.get("window").width;
const screenHeight = Dimensions.get("window").height;

export default class ContactUsScreen extends React.Component {
    state = {

    }

    render() {
        return (
            <View style={styles.container}>
                <View style={styles.header}>
                    <TouchableOpacity style={styles.chevronContainer} onPress={()=>this.props.navigation.goBack()}>
                        <Image source={require('../assets/chevron_left.png')} style={styles.chevronLeft}/>
                    </TouchableOpacity>          
                    <Text style={styles.headerFont}>Contact Us</Text>
                </View>
                <View style={{marginTop:10}}>
                    <Image source={require('../assets/sample_banner.png')} style={{width:screenWidth-20,height:150,borderRadius:20}}></Image>
                </View>
                <View style={{alignSelf:"center", marginLeft:15,marginTop:20,flexDirection:'row'}}>
                    <TouchableOpacity onPress={()=>Linking.openURL('mailto:firdaus531@gmail.com')} style={{marginRight:30}}>
                        <Image style={{height:70,width:70}} source={require('../assets/email2.png')}></Image>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={()=>Linking.openURL('instagram://user?username=firdausfdz_')} style={{marginRight:30}}>
                        <Image style={{height:70,width:70}} source={require('../assets/instagram.png')}></Image>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={()=>Linking.openURL('twitter://user?screen_name=damnus98')} style={{marginRight:30}}>
                        <Image style={{height:70,width:70}} source={require('../assets/twitter.png')}></Image>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: "center",
        // backgroundColor: "rgb(251,82,87)",
    },
    header: {
        backgroundColor: "white",
        width: screenWidth,
        height: 50,
        justifyContent: "center",
        backgroundColor: "rgb(251,82,87)",
        flexDirection:"row",
    },
    headerFont: {
        fontWeight: "bold",
        alignSelf: "center",
        fontSize: 24,
        color: "white",
    },
    chevronContainer: {
        alignSelf: "center",
        position:'absolute',
        left:20,
    },
    chevronLeft: {
        width: 20,
        height: 20,
    },
})